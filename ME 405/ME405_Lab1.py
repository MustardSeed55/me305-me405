'''
@file ME405_Lab1.py

@details This program simulates a vending machine. Interactive messages appear
         in the user's Console and the user inputs commands to add money, get
         change, and purchase beverages.
         
@author Dakota Baker
@date January 12, 2021
'''
import time
import keyboard
import sys

## @brief This variable controls the state of the Finite State Machine
state = 0
## @brief n = 1 represents a user has pushed a drink button
n = 0
## @brief Index for general scrolling/idle text messages
i = 0
## @brief Index for extra drink scrolling/idle text messages
k = 0
## @brief Usable money in vending machine
balance = 0
## @brief Used to identitfy beverage type
beverage = None
## @brief Price of a cuke, $1.00
cuke = 100
## @brief Price of a popsi, $1.20
popsi = 120
## @brief Price of a spryte, $0.85
spryte = 85
## @brief Price of a dr. pupper, $1.10
pupper = 110
## @brief Represents current price for a given transaction
price = 0
## @brief Stores user's inputed command
pushed_key = None


def on_keypress(thing):
    '''
    @brief Callback which runs when the user presses a key.
    '''
    global pushed_key
    pushed_key = thing.name

def updateDisplay(balance):
    '''
    @brief Updates vending machine display with current balance
    @param balance Usable money in vending machine
    '''
    print('Balance: ${:0.2f}'.format(balance/100))

def lowest_Denominator_change(change):
    '''
    @brief Returns a tuple of monetary denominations for a given integer 
    @param change The change to be received after a transaction as an integer
                  (0 to infinity). Thus, pennies are 1's.
    @return Returns the change as a tuple: (penny, nickle, dime, quarter, dollar, 
                                            5 dollar, 10 dollar, 20 dollar).
    '''
    # int will round-down a decimal to the nearest integer.
    # Thus, number will be 0 if the change is less than what is needed
    # for a certain denomination.
    dollar_20 = int(change/2000)
    change = change - dollar_20*2000
    dollar_10 = int(change/1000)
    change = change - dollar_10*1000
    dollar_5 = int(change/500)
    change = change - dollar_5*500
    dollar_1 = int(change/100)
    change = change - dollar_1*100
    quarters = int(change/25)
    change = change - quarters*25
    dimes = int(change/10)
    change = change - dimes*10
    nickles = int(change/5)
    change = change - nickles*5
    pennies = int(change/1)
    ## @brief The user's change as a tuple
    change_tup = (pennies, nickles, dimes, quarters, dollar_1, dollar_5, dollar_10, dollar_20)
    return change_tup           

keyboard.on_press(on_keypress)  ########## Set callback
      
###############################################################################

while True:
    # State 0: Initialize
    if state == 0:
        print('------------------------------------------------------------\n' +
              '\t\t\t\tWelcome to Vendotron!\n' +
              'User commands:\n' +
              '\t0 = Penny\n' +
              '\t1 = Nickle\n' +
              '\t2 = Dime\n' +
              '\t3 = Quarter\n' +
              '\t4 = $1\n' +
              '\t5 = $5\n' +
              '\t6 = $10\n' +
              '\t7 = $20\n\n' +
              '\te = Eject Change / Cancel Transaction\n' +
              '\ty = Yes\n' +
              '\tn = No\n\n' +
              'These are the available beverages:\n' +
              '\tc = Cuke       ($1.00)\n' +
              '\tp = Popsi      ($1.20)\n' +
              '\ts = Spryte     ($0.85)\n' +
              '\td = Dr. Pupper ($1.10)\n' +
              '------------------------------------------------------------\n')
        updateDisplay(balance)
        state = 1
        ## @brief Starts a running timer as reference for when to start idle messages
        timer_start = time.time()
        
        
    # State 1: Wait for Command
    elif state == 1:
        ## @brief Current time of FSM. Used in reference to timer_start.
        current_time = time.time()
        if k >= 1:
            if current_time - timer_start > 10 and k == 1:
                print('Please enter y or n')
                k = 2
            elif current_time - timer_start > 35 and k == 2:
                print('Please enter y or n')
                k = 3
            elif current_time - timer_start > 60 and k == 3:
                k = 0
                price = 0
                state = 3
            else:
                pass
        else:
            if current_time - timer_start > 10 and i == 0:
                print("Buy some Popsi! It's surprisingly delicious!\n")
                i = 1
            elif current_time - timer_start > 20 and i == 1:
                print("You're safe with a Dr. Pupper.\nIt's ok. He's a doctor.\n")
                i = 2
            elif current_time - timer_start > 30 and i == 2:
                state = 0
                i = 0
                timer_start = time.time()
            else:
                pass
        if pushed_key:
            if pushed_key == 'c':
                if balance >= cuke:
                    state = 2
                    beverage = 'c'
                else:
                    print('Funds Insufficient')
            elif pushed_key == 'p':
                if balance >= popsi:
                    state = 2
                    beverage = 'p'
                else:
                    print('Funds Insufficient')
            elif pushed_key == 's':
                if balance >= spryte:
                    state = 2
                    beverage = 's'
                else:
                    print('Funds Insufficient')
            elif pushed_key == 'd':
                if balance >= pupper:
                    state = 2
                    beverage = 'd'
                else:
                    print('Funds Insufficient')
            elif pushed_key == '0':
                balance += 1
                updateDisplay(balance)
            elif pushed_key == '1':
                balance += 5
                updateDisplay(balance)
            elif pushed_key == '2':
                balance += 10
                updateDisplay(balance)
            elif pushed_key == '3':
                balance += 25
                updateDisplay(balance)
            elif pushed_key == '4':
                balance += 100
                updateDisplay(balance)
            elif pushed_key == '5':
                balance += 500
                updateDisplay(balance)
            elif pushed_key == '6':
                balance += 1000
                updateDisplay(balance)
            elif pushed_key == '7':
                balance += 2000
                updateDisplay(balance)
            elif pushed_key == 'e':
                price = 0
                state = 3
            elif n == 1:
                if pushed_key == 'y':
                    state = 0
                    k = 0
                    n = 0
                elif pushed_key == 'n':
                    state = 3
                    k = 0
                    n = 0
                else:
                    print('Invalid Command')
                    print('Would you like another drink? (y/n)')
            else:
                print('Invalid command')
            
            pushed_key = None
            i = 0
            timer_start = time.time()
        
        
    # State 2: Get Beverage
    elif state == 2:
        n = 1
        k = 1
        if beverage == 'c':
            price = cuke
            balance = balance - price
            updateDisplay(balance)
            print("Here's your Cuke")
            print('Would you like another drink? (y/n)')
            price = cuke
        elif beverage == 'p':
            price = popsi
            balance = balance - price
            updateDisplay(balance)
            print("Here's your Popsi")
            print('Would you like another drink? (y/n)')
        elif beverage == 's':
            price = spryte
            balance = balance - price
            updateDisplay(balance)
            print("Here's your Spryte")
            print('Would you like another drink? (y/n)')
        elif beverage == 'd':
            price = pupper
            balance = balance - price
            updateDisplay(balance)
            print("Here's your Dr. Pupper")
            print('Would you like another drink? (y/n)')
        else:
            print('ERROR: Invalid beverage')
            sys.exit()
        state = 1
        timer_start = time.time()
        
            
    # State 3: Eject Change
    elif state == 3:
        print('Thank you for your business!')
        print("Here's your change: ${:0.2f}".format(balance/100))
        print('(P, N, D, Q, $1, $5, $10, $20)')
        change = lowest_Denominator_change(balance)
        print(change)
        balance = 0
        n = 0 
        k = 0
        state = 0
        