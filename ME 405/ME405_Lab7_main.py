'''
@file ME405_Lab7_main.py

@brief Initializes ME405_Lab7.py.
@details Initializes ME405_Lab7.py to read the x, y, & z (is there contact) location
         of a ball on the rotating platform's touchscreen.
@author Dakota Baker
@date February 24, 2021
'''

from pyb import Pin
from ME405_Lab7 import TouchScreen
import micropython

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)

## Pin Object for x_minus reading on the touchscreen
pin_xm = Pin(Pin.cpu.A6)
## Pin Object for x_plus reading on the touchscreen
pin_xp = Pin(Pin.cpu.A0)
## Pin Object for y_minus reading on the touchscreen
pin_ym = Pin(Pin.cpu.A7)
## Pin Object for y_plus reading on the touchscreen
pin_yp = Pin(Pin.cpu.A1)
## Length of the touchscreen (x direction)
length = 176    # [mm]
## Width of the touchscreen (y direction)
width = 99.36   # [mm]

## A task that uses the TouchScreen class to collect touchscreen data
task1 = TouchScreen(pin_xm, pin_xp, pin_ym, pin_yp, length, width)

while True:
    print(task1.Scan_XYZ())
# times = 0
# for n in range(100):
#     times += task1.Scan_XYZ()
# times = times/100
# print(times)

