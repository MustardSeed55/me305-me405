'''
@file ME405_Lab4_Plot.py

@brief Plots the Nucleo's internal temperature and the room's ambient temperature

@author Dakota Baker
@date February 8, 2021
'''

import array
import matplotlib.pyplot as plt

time = array.array('f')

Nucelo_temp = array.array('f')

Ambient_temp = array.array('f')

with open('Temperature_Data.csv') as file:
    while True:
        line = file.readline()
        if line == '':
            break
        else:
            (t, N, A) = line.strip().split(',')
            time.append(int(t)/60)
            Nucelo_temp.append(float(N))
            Ambient_temp.append(float(A))
        

# Using matplotlib.pyplot
plt.figure(1)
plt.plot(time, Nucelo_temp, 'k-')
plt.plot(time, Ambient_temp, 'k--')
plt.grid(True)
plt.title('Temperature using a Nucleo and MPC9808 Sensor')
# plt.yticks([0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.3])
# plt.xticks([0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0])
plt.ylabel('Temperature [C]')
plt.xlabel('Time [hr]')
plt.legend(['Nucelo Internal Temperature', 'Ambient Room Temperature'])
print('Plot created')