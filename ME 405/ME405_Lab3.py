'''
@file ME405_Lab3.py

@brief Plots the response of the USER button being pressed on a Nucelo
@details This file interacts with a Nucleo board and uses the USER button.
         When the button is pressed, an analog-to-digital conversion of the
         signal response is performed on the Nucelo and sent to a user's 
         computer through serial communication. This step response is then plotted.
         
@author Dakota Baker
@date January 29, 2021
'''

import serial
import matplotlib.pyplot as plt
import numpy as np
import array

## Sets-up serial communication with the Nucleo
ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)

## Holds the information for the user's input
user_input = None
while user_input != 'G':
    user_input = input('Press G to go: ')
    if user_input == 'G':
        ser.write(user_input.encode('ascii'))
        print('Press USER Button')
    else:
        pass

## Holds the data for the time and ADC values
data_list = []
## Holds the data for time. Formed from data_list[0].
time = []
## Holds the data for adc count. Formed from data_list[1].
ADC_count = []

while True:
    # Nucleo is sending strings in the form 'time, ADC output\n'
    try:
        ## Rceives the string of time and adc values from the Nucleo
        data_string = ser.readline().decode('ascii')    # Decode serial communication using Ascii standards
        if data_string == 'D\n':
            print('Data Collected')
        elif data_string == 'G\n':
            print('Transfering Data...')
        elif data_string == 'S\n':
            break
        else:
            ## A list of the data_string once it is stripped of \n characters and split at commas
            data_list = data_string.strip().split(',')
            time.append(float(data_list[0]))         # Convert time portion of data_list into a floating point number
            ADC_count.append(float(data_list[1]))    # Convert ADC output portion of data_list into a floating point number
    except KeyboardInterrupt:
        break
    except:
        pass
    
ser.close()

## Index where USER button is initially pressed (output 0)
start_index = ADC_count.index(0)

if start_index != 0:
    for n in range(start_index):
        # Remove all data points before button press
        time.pop(0)
        ADC_count.pop(0)
    for n in range(len(time)):
        # Zero time array
        time[n] = time[n] - start_index
        # Adjust time array to ms when original read at 5 us
        time[n] = time[n]*5/1000
        # Convert ADC to volts
        ADC_count[n] = ADC_count[n]*(3.3/4095)
else:
    pass

# Create a .csv file from time and position lists

## Creates a tuple of time and adc values to be used with np.colum_stack()
data_tup = (time, ADC_count)
## Creates an array of 2 columns with as many rows as data points that were collected
data_CSV = np.column_stack(data_tup)
np.savetxt('USER_Button.csv', data_CSV, fmt = '%f', delimiter = ',')
print('USER_Button.csv created in current folder')

## An array that represents a step function
step_function = array.array('f', (3.3 for n in range(len(time))))
# if start_index != 0:
#     for n in range(start_index):
#         step_function[n] = 0
# else:
step_function[0] = 0


# Using matplotlib.pyplot
plt.figure(1)
plt.plot(time, ADC_count, 'ko', markersize = 3)
plt.plot(time, step_function, 'k--')
plt.grid(True)
# plt.title('USER Button Response on a Nucelo')
plt.yticks([0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.3])
# plt.xticks([0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0])
plt.ylabel('Volts [V]')
plt.xlabel('Time [ms]')
plt.legend(['USER Button', 'Step Function'])
print('Plot created')
