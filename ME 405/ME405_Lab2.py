'''
@file ME405_Lab2.py

@details An LED will blink at a random time. The user must push the USER button
         on the Nucleo board. A reaction time will be printed in the console.
         Note: Can use the utime module or pyboard timers.
@author Dakota Baker
@date January 22, 2021
'''

import pyb
import micropython
import random
# import utime

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)

print('---------------------------------------------------------\n' +
      'A green LED will blink 3 times.\n' +
      'There will be a pause between 2-3 seconds.\n' +
      'Once you see the LED turn on again, push the USER button.\n' +
      'User\'s reaction time will be displayed in the console.\n' +
      'Ctrl+c to exit\n'
      '---------------------------------------------------------\n')

## Timer count when LED turns on
start_count = 0
## Timer count when user presses USER button
end_count = 0
# start_time = 0
# end_time = 0
## Timer period
tim_period = int(7e6)
## User reaction [microseconds]
reaction_time = 0
## Stores previous reaction times to average at termination of program
reaction_list = []


def ISR(pinSource):
    '''
    @brief Interrupt Service Routine from Pin C13 (USER Button)
    @details Interrupt Service Routine callback function. Runs when the USER 
             button is pressed down and retrieves the timer counter at that instant.
    @param pinSource Pin which caused the interrrupt (C13)
    '''
    global end_count
    end_count = timer2.counter()
    # global end_time
    # end_time = utime.ticks_us()
        
## Initializes PinA5, which is an LED on the Nucelo board, as push/pull
PinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
PinA5.low()    # Start with the LED off
## Sets-up a timer at 80 MHz. Every tick is counted and the period is stored in the tim_period variable.
# Resolution = 1 microsecond; thus, prescaler = (1 us)*(80 MHz) = 80
# Want T = 7 sec; thus, PER = T*(80 MHz)/prescaler = 1.5e6
timer2 = pyb.Timer(2, prescaler = 79, period = tim_period)
## Amount of time for a complete timer cycle before it overflows
period_time = int(7e6)    # microseconds, 7 sec


## Initializes an external interrupt on the falling edge for PinC13 (USER button)
extint = pyb.ExtInt(pyb.Pin.cpu.C13,
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.OUT_PP, 
                    callback = ISR)

while True:
    try:
        pyb.delay(2000)    # Wait 2 seconds
        
        for n in range(6):    # 3 blinks in 1 second
            PinA5.value(1 - PinA5.value())
            pyb.delay(167)    
        
        # All USER button pressed not registered until now
        end_count = 0
        # end_time = 0
        
        ## An integer between 2000 & 3000 miliseconds (2-3 sec) to delay the LED from turning on.
        random_delay = random.randrange(2000, 3000)
        pyb.delay(random_delay)    # 2-3 seconds

        # Reset timer for new cycle        
        timer2.counter(0)
        
        if end_count != 0:
            print('FALSE START!')
        else:
            PinA5.high()
            start_count = timer2.counter()
            # start_time = utime.ticks_us()
            pyb.delay(1000)
            PinA5.low()
            if end_count == 0:
            # if end_time == 0:
                print('SCRATCH: USER button wasn\'t pressed')
            else:
                ## Difference between ending timer tick and starting timer tick. Accounts for overflow if negative difference.
                reaction_count = end_count - start_count
                if reaction_count < 0:
                    reaction_count += tim_period
                else:
                    pass
                reaction_time = int(reaction_count*(period_time/tim_period))
                # reaction_time = int(utime.ticks_diff(end_time, start_time))
                reaction_list.append(reaction_time)
                print('Reaction Time: {:} microseconds'.format(reaction_time))

        pyb.delay(1000)
    except KeyboardInterrupt:
        PinA5.low()
        ## Sum of all reaction times
        reaction_sum = 0
        ## Length of reaction_list
        reaction_length = len(reaction_list)
        if reaction_length == 0:
            reaction_average = 0
        else:
            for n in range(reaction_length):
                reaction_sum += reaction_list[n]
            ## Average of reaction times. Uses reaction_sum and reaction_length
            reaction_average = int(reaction_sum / reaction_length)
        print('Average Reaction Time: {:} microseconds'.format(reaction_average))
        break
