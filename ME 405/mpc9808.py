'''
@file mpc9808.py

@brief Initializes a I2C connection with a MPC9808 sensor.
@details This class uses the manufacturer's ID and memory address to check if 
         the sensor is properly connected. The slave's (sensor) address is used
         to communicate with the sensor and the read memory address tells the
         sensor what data the user wants to read. Temperatures can be read in 
         Celsius and Fahrenheit.
@author Dakota Baker
@date February 6, 2021
'''

from pyb import I2C
import pyb
import micropython

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)

class MPC9808_I2C:
    '''
    @brief Initializes a I2C connection with a MPC9808 sensor.
    @details This class uses the manufacturer's ID and memory address to check if 
         the sensor is properly connected. The slave's (sensor) address is used
         to communicate with the sensor and the read memory address tells the
         sensor what data the user wants to read. Temperatures can be read in 
         Celsius and Fahrenheit.
    '''
    
    def __init__(self, Manufacturer_Memaddr, Manufacturer_ID, Slave_Addr, Read_Memaddr):
        '''
        @brief Creates object copies of the input parameters.
        @param Manufacturer_Memaddr The memory address of the manufacturer's ID
        @param Manufacturer_ID The value stored in the manufacturer's memory address
        @param Slave_Addr The address of the sensor used in I2C communication. Note: This value changes depending on how the sensor is wired to the Nucleo.
        @param Read_Memaddr The address of what data the user wants to read. 5 = Ambient Temperature.
        '''
    
        ## Class object of the Nucleo I2C Master object
        self.Nucleo_i2c = I2C(1, I2C.MASTER)
        ## Class object of the manufacturer memory address 
        self.Manufacturer_Memaddr = Manufacturer_Memaddr
        ## Class object of the manufacturer ID
        self.Manufacturer_ID = Manufacturer_ID
        ## Class object of the slave (sensor) address
        self.Slave_Addr = Slave_Addr
        ## Class object of the memory address the user wants to read
        self.Read_Memaddr = Read_Memaddr
    
    def celsius(self):
        '''
        @brief Reads the sensor's ambient temperature in degrees celcius.
        '''
            
        data = self.Nucleo_i2c.mem_read(2, addr = self.Slave_Addr, memaddr = self.Read_Memaddr)
        [x, data_byte1] = bin(data[0]).split('b')
        [x, data_byte2] = bin(data[1]).split('b')
        # print(data_byte1)
        # print(data_byte2)
        data_float = 0
        byte2_size = len(data_byte2)
        # print(byte2_size)
        for n in range(byte2_size):
            data_float += int(data_byte2[n])*(2**((byte2_size-5) - n))
        # print(data_float)
        n = 0
        byte1_size = len(data_byte1) - 4
        for n in range(byte1_size):
            data_float += int(data_byte1[n+4])*(2**((byte1_size+3) - n))
        if data_byte1[3] == 1:
            data_float = -data_float
            return data_float
        else:
            return data_float
        
    def fahrenheit(self):
        '''
        @brief Reads the sensor's ambient temperature in degrees fahrenheit.
        '''
        
        data_float = self.celsius()
        data_float = (9/5)*data_float + 32
        return data_float

            
    def check(self):
        '''
        @brief Checks if the I2C communication is properly established by reading the manufacturer's ID.
        '''

        ID_Check = bytearray( self.Nucleo_i2c.mem_read(2, addr = self.Slave_Addr, memaddr = self.Manufacturer_Memaddr) )
        if ID_Check[1] == self.Manufacturer_ID:
            # print('I2C Object is Recognized')
            return True
        else:
            # print('ERROR: I2C Object NOT Recognized')
            return False
            
        
        
if __name__ == "__main__":
    '''
    @brief Test Code
    '''
    Manufacturer_Memaddr = 6
    Manufacturer_ID = 84
    Slave_Addr = 25
    Read_Memaddr = 5
    task1 = MPC9808_I2C(Manufacturer_Memaddr, Manufacturer_ID, Slave_Addr, Read_Memaddr)
    task1.check()
    while True:
        try:
            print('{:0.3f} deg C, {:0.3f} deg F'.format(task1.celsius(), task1.fahrenheit()))
            pyb.delay(1000)
        except:
            print('Program exited')
            break

