'''
@file homework1.py
@author Dakota Baker
@date January 11, 2021

@details This function returns change, as a tuple (penny, nickle, dime, quarter, 
         dollar, 5 dollar, 10 dollar, 20 dollar), in the fewest denominations 
         possible for a given price and payment.
'''

def getChange(price, payment):
    '''
    @brief Returns change for a given price and payment
    @param price The price for a given object (0 to infinity) as an integer.
                 Thus, pennies are 1's.
    @param payment The payment given for the object as a size 8 tuple.
                   (penny, nickle, dime, quarter, dollar, 5 dollar, 10 dollar, 20 dollar).
    @return Returns the change as a tuple: (penny, nickle, dime, quarter, dollar, 5 dollar, 10 dollar, 20 dollar).
    '''
    ## @brief Integer representing the price of an object
    price = int(price)
    print('Price   = $' + str(price) + ' = $' + str(price/100))
    
    if price < 0 or payment[0] < 0 or payment[1] < 0 or payment[2] < 0 or\
        payment[3] < 0 or payment[5] < 0 or payment[6] < 0 or payment[7] < 0:
        print('ERROR! Price and payment must be positive numbers.')
        
        return False
           
    else:
        # tuple = (penny, nickle, dime, quarter, dollar, 5 dollar, 10 dollar, 20 dollar)
        ## @brief The payment of an object as a number
        payment_num = payment[0]*1 + payment[1]*5 + payment[2]*10 + payment[3]*25 +\
                      payment[4]*100 + payment[5]*500 + payment[6]*1000 + payment[7]*2000
        print('Payment = $' + str(payment_num) + ' = $' + str(payment_num/100))
    
        change = (payment_num - price)
        print('Change  = $' + str(change) + '  = $' + str(change/100))
    
        if change >= 0:
            # int will round-down a decimal to the nearest integer.
            # Thus, number will be 0 if the change is less than what is needed
            # for a certain denomination.
            dollar_20 = int(change/2000)
            change = change - dollar_20*2000
            dollar_10 = int(change/1000)
            change = change - dollar_10*1000
            dollar_5 = int(change/500)
            change = change - dollar_5*500
            dollar_1 = int(change/100)
            change = change - dollar_1*100
            quarters = int(change/25)
            change = change - quarters*25
            dimes = int(change/10)
            change = change - dimes*10
            nickles = int(change/5)
            change = change - nickles*5
            pennies = int(change/1)
            
            ## @brief The user's change as a tuple
            change_tup = (pennies, nickles, dimes, quarters, dollar_1, dollar_5, dollar_10, dollar_20)
            print(change_tup)
            
            return change_tup
           
        else:
            print('ERROR: Payment is not enough!')
            change = -change
            print('Need ${:} more'.format(change/100))
            
            return False
    
    
    
if __name__ == '__main__':
    price = 10001
    payment = (52, 0, 0, 0, 2, 2, 0, 5)
    change = getChange(price, payment)