'''
@file ME405_Lab4_main.py

@brief Reads the Nucleo's internal temperature and the ambient temperature from a MPC9808 sensor every 60 seconds.
@details Initializes an I2C connection with a MPC9808 sensor using the mpc9808.py file.
         Additionally, ADCALL() is used to read the internal temperature of the Nucelo.
         Both temperatures are read in Celsius every 60 seconds and are stored in a CSV file
         called Temperature_Data.csv.
         Note: File must be named "main.py" to run on Nucleo start-up.
@author Dakota Baker
@date February 6, 2021
'''

import pyb
import micropython
import array
from mpc9808 import MPC9808_I2C

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)

## Variable that represents whether the USER button has been pushed or not
button_press = False

def ISR(pinSource):
    '''
    @brief Function that runs when the USER button is pressed down
    @details Upon the USER button being pressed, the variable button_press becomes True
    '''
    global button_press
    button_press = True

## Initializes an external interrupt on the falling edge for PinC13 (USER button)
extint = pyb.ExtInt(pyb.Pin.cpu.C13,
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.PULL_NONE, 
                    callback = ISR)

## Initialize Nucleo pin A5 to turn off or on
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
pinA5.low()
pyb.delay(2000)
    
## Establishes an analog-to-digital converter
adcall = pyb.ADCAll(12, 0x70000)

# This value isn't necessary, but it gets the adcall calculations working for temp.
adcall.read_core_vref()

## Stores all the internal temperature data for the Nucleo
temp_data = array.array('f')

## Stores the ambient temperature data
sensor_temp_data = array.array('f')

## Stores the elapsed time since data collection started
time = array.array('H')

## Memory Address for the Manufacturer ID on the MPC9808 Sensor
Manufacturer_Memaddr = 6
## Value stored in the Manufacturer ID on the MPC9808 for reference
Manufacturer_ID = 84
## Address of the MPC9808 sensor. Note: This is dependent on how the sensor is connected to the Nucleo.
Slave_Addr = 25
## Memory Address on the MPC9808 sensor of what to read. 5 = Ambient Temperature.
Read_Memaddr = 5

## Task to read the MPC9808 sensor's data
Sensor_Task = MPC9808_I2C(Manufacturer_Memaddr, Manufacturer_ID, Slave_Addr, Read_Memaddr)

if Sensor_Task.check() == True:
    for i in range(6):    # 3 blinks in 1 second
        pinA5.value(1 - pinA5.value())
        pyb.delay(167)
    pinA5.low()
    
    ## Counting index
    n = 0
    
    with open('Temperature_Data.csv', 'w') as file:
        while True:
            try:
                time.append(n)
                temp_data.append(adcall.read_core_temp())
                sensor_temp_data.append(Sensor_Task.celsius())
                # print('{:}, {:}, {:}'.format(time[n], temp_data[n], sensor_temp_data[n]))
                file.write('{:}, {:}, {:}\r'.format(time[n], temp_data[n], sensor_temp_data[n]))
                n += 1
                if button_press == True:
                    pinA5.high()
                    pyb.delay(1000)
                    pinA5.low()
                    break
                else:
                    pyb.delay(60000)    # Delay 1 sec
            except KeyboardInterrupt:
                break
else:
    print('ERROR: Temperature Sensor not ready')