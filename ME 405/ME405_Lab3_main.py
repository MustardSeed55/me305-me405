'''
@file ME405_Lab3_main.py

@brief Converts analog USER button response to a digital signal
@details When the USER button is pressed, the analog response is converted
         to a digital value using the adc module. This information is then 
         sent via serial communication with a user's computer.
         Note: Must be named "main.py" to run on Nucleo start-up.
'''

import pyb
import micropython
import array

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)

## Sets-up serial port so Nucelo can communicate with a user's computer
ser = pyb.UART(2)

## Global variable to represent state of USER button: pressed or not
button_press = False


def ISR(pinSource):
    '''
    @brief Function that runs when the USER button is pressed down
    @details Upon the USER button being pressed, the variable button_press becomes True
    '''
    global button_press
    button_press = True
    
## Holds the information for the user's input from the computer
user_input = None
while user_input != 'G':
    if ser.any():
        user_input = ser.readline().decode('ascii')
        if user_input == 'G':
            break
        else:
            pass

## Initializes an external interrupt on the falling edge for PinC13 (USER button)
extint = pyb.ExtInt(pyb.Pin.cpu.C13,
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.PULL_NONE, 
                    callback = ISR)


## Establishes an analog-to-digital converter on Pin A5
adc = pyb.ADC(pyb.Pin.board.PC0)    # Pin A5
## Timer used for ADC
timer6 = pyb.Timer(6, freq = 200000)
## Buffer to hold data for ADC
buffer = array.array('H', (0 for n in range(1000)))
## Buffer to hold data before button press
pre_buffer = array.array('H', (0 for n in range(300)))


while True:
    if button_press == True:
        if adc.read() > 100:
            adc.read_timed(buffer, timer6)
            ser.write('D\n')
            break
    else:
        adc.read_timed(pre_buffer, timer6)

ser.write('G\n')     

## Length of the pre_buffer array
pre_length = len(pre_buffer)

for n in range(pre_length):
    ser.write('{:},{:}\n'.format(n, pre_buffer[n]))
for n in range(len(buffer)):
    ser.write('{:},{:}\n'.format(n+pre_length, buffer[n]))

ser.write('S\n')
