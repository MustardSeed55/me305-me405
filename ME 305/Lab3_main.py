'''
@file Lab3_main.py
@brief Calls Task_Encoder.py and Task_UI.py to read and set encoder position
'''

#import time
from Task_Encoder import Encoder
from Task_UI import UI
import pyb

## Pyboard timer object
timer = 3

## Pin object 1
pin1 = pyb.Pin(pyb.Pin.cpu.A6)

## Channel of timer for Pin Object 1
pin1_ch = 1

## Pin Object 2
pin2 = pyb.Pin(pyb.Pin.cpu.A7)

## Channel of timer for Pin Object 2
pin2_ch = 2

## Size of counter (bits). The Nucelo has a 16-bit counter
counter_size = 16 # bit size

## Pulse per revolution for user's encoder
PPR = 112 # Pulse per revolution

## Maximum revolutions per minute for user's motor
rpm = 6000 # maximum rpm of motor

## Task Object that runs the encoder
task1 = Encoder(timer, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm)

## Task Object that runs the user interface
task2 = UI(task1)

## Determines the desired time to end running the tasks in seconds \
#t_end = time.time() + 1    # End time is 20 seconds after the start
#while time.time() < t_end:  # Will run tasks 1 and 2 until the end time has been reached
while True:
    task1.run()
    task2.run()