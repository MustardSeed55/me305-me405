'''
@file Task_UI.py

This file controls the user interface. Prompts user commands and displays the result of each command
'''

from pyb import UART
import utime
import sys

class UI:
    '''
    @brief Controls the user interface by giving a list of commands and prompts the user to enter a command
    '''
    
    ## Constant defining State 0: Initializing Program
    S0_Init   = 0
    
    ## Constant defining State 1: Scan for user input
    S1_Scan   = 1
    
    def __init__(self, task1_Encoder):
        '''
        @brief Prints the list of user commands and initializes FSM
        @param task1_Encoder This is task1 from Lab3_main.py. It is inputed as a parameter so the encoder's functions can be called
        '''
        
        print('-----------------------------------------------------\n' +
              'User commands:\n' +
              '\tz = Zero the encoder position\n' +
              '\tp = Print out the encoder position [degrees]\n' +
              '\td = Print out the encoder delta [pulses]\n' +
              '\tq = quit program\n\n'
              'Enter command: ')
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_Init
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## A constant defining the starting time of the program in miliseconds
        self.start_time = utime.ticks_ms()  # The number of miliseconds since arbitrary starting pont
        #self.start_time = time.time()      # The number of seconds since Jan 1. 1970 
        
        ## A constant defining the time period this program will wait before re-running
        self.period = 100 # miliseconds
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.period)
        #self.next_time = self.start_time + self.period
        
        ## Defines a copy of the Encoder class cooperatively running so the UI can call its attriutes like get_position()
        self.Encoder = task1_Encoder
        
        ## Sets-up UART so UI can read user input
        self.uart = UART(2)
        
    def run(self):
        '''
        @brief Scans for user input and then calls the appropriate Encoder function
        '''
        ## Current time (in miliseconds) since an arbitrary start time (same instant as variable self.start_time)
        self.current_time = utime.ticks_ms() 
        #self.current_time = time.time()
        if utime.ticks_diff(self.current_time, self.next_time) >= 0 or self.runs == 0:
        #if (self.current_time >= self.next_time) or self.runs == 0:
            if self.state == self.S0_Init:
                #print(str(self.runs) + ' State 0: Initialize     - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                self.transitionTo(self.S1_Scan)
                
            elif self.state == self.S1_Scan:
                #print(str(self.runs) + ' State 1: Scan           - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                if self.uart.any():
                    keyboard = self.uart.readchar()
                    if keyboard == 122: # z
                        self.Encoder.set_position(0)
                        print('Encoder zeroed')
                        self.Encoder.get_position()
                        keyboard = 0
                    elif keyboard == 112: # p
                        self.Encoder.get_position()
                        keyboard = 0
                    elif keyboard == 100: # d
                        print('Previous delta = {:}'.format(self.Encoder.delta))
                        keyboard = 0
                    elif keyboard == 113: # q
                        print('--------------------Program Ended--------------------')
                        sys.exit()
                    else:
                        print('Invalid command')
                    print('\nEnter command: ')

            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.period)   
            #self.next_time += self.period
            
            
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the UI is transitioning to
        '''
        self.state = newState
