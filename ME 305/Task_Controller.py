'''
@file Task_Controller.py

This file is initiated by Lab6_main.py*.

This task drives the motor, uses the encoder to discern the actual angular velocity,
and uses a closed-loop controller to manipulate the motor's response.

*Lab6_main.py must be named main.py on the Nucleo for it to run on boot-up.
'''

import pyb
from pyb import UART
import utime
import sys
import math

class Controller:
    '''
    @brief Controls the DC motor
    '''
    
    ## Constant defining State 0: Initializing Program
    S0_INIT           = 0
    
    ## Constant defining State 1: Wait for command from user interface
    S1_WAIT_FOR_CMD   = 1
    
    ## Constant defining State 2: Get encoder position and timestamp
    S2_GET_DATA       = 2
    
    ## Constant defining State 3: Get user K value
    S3_GET_K          = 3
    
    ## Constant defining State 4: Get user Omega_ref
    S4_GET_Omega_ref      = 4
    
    
    def __init__(self, task1_Encoder, task2_Motor, dbg):
        '''
        @brief Sets-up Finite State Machine and serial communication with user
        @param task1_Encoder Inputting the Encoder class as a parameter so Encoder
                             specific functions can be called.
        @param task2_Motor  Inputting the Motor class as a parameter so Motor 
                            specific functions can be called.
        @param dbg          1 = print debug statement, 0 = no debug statements
        '''
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0

        ## Sets-up serial port so Nucelo can communicate with a user's computer
        self.ser = UART(2)
        
        ## Creates object for encoder class
        self.Encoder = task1_Encoder
        
        ## Creates object for motor class
        self.Motor = task2_Motor
        
        if self.Encoder.period_max > 100: # 100 milisecond
            ## A constant defining the time period this program will wait before re-running. If period_max is less than 1 milisecond, this program has to use utime.ticks.us()
            self.period = 100
        elif self.Encoder.period_max < 100 and self.Encoder.period_max > 1:
            self.period = self.Encoder.period_max
        else:
            print('ERROR: period is too small. Need to use utime.ticks.us()')
            print('-------Program Ended-------')
            sys.exit()
        
        ## A constant defining the starting time of the program in miliseconds
        self.start_time = utime.ticks_ms()  # The number of miliseconds since arbitrary starting point
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.period)
        
        ## Timestamp for encoder position
        self.time = 0
        
        ## Encoder angular velocity
        self.Omega = 0
        
        ## Object for debug statements
        self.dbg = dbg

    def run(self):
        '''
        @brief Runs at 4 states: (0) Initialize, (1) Wait for user command, (2) Get motor data, (3) Get user K value, (4) Get user desired Ω
        @details Upon boot-up, Controller() automatically goes into the WAIT_FOR_CMD state. From this state, it will transition
                 to State 3: GET_K or State 4: GET_Ω_ref to acquire those values. Working with Lab6_UI.py, the program will
                 not run unless both K and Ω_ref are entered. When the user enters 'G' into the console, Controller() goes to 
                 State 2: GET_DATA state where the motor is set into motion based on the error from the closed-loop equation. 
                 The time stamps and actual angular velocities are immediately sent to the user through serial communication.
                 After 3 seconds, or if the user enters 'S' into the console, the program ends. 
        '''
        
        ## Current time (in miliseconds) since an arbitrary start time (same instant as variable self.start_time)
        self.current_time = utime.ticks_ms() 
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0 or self.runs == 0):
            if self.state == self.S0_INIT:
                # self.Omega_ref = int(input('Omega_ref = '))
                # self.User_Kp = float(input('Kp = '))
                self.transitionTo(self.S1_WAIT_FOR_CMD) 
            
            elif self.state == self.S1_WAIT_FOR_CMD:
                if self.dbg:
                    print('State 1: Wait for Command')
                if self.ser.any():
                    val = self.ser.readline().decode('ascii')
                    if val == 'K':
                        self.transitionTo(self.S3_GET_K)
                    elif val == 'W':
                        self.transitionTo(self.S4_GET_Omega_ref)
                    elif val == 'G':
                        ## Create object for ClosedLoop controller
                        self.ClosedLoop = ClosedLoop(self.User_Kp, self.Omega_ref, self.period) 
                        self.transitionTo(self.S2_GET_DATA)
                        ## Documents the time when Controller() starts to collect data so data collection can stop after 3 seconds
                        self.run_time = utime.ticks_ms()
                        self.Motor.enable()
                        if self.dbg:
                            print('Transition to Get Data')
                        
            elif self.state == self.S2_GET_DATA:
                self.Motor.set_duty(self.ClosedLoop.run(self.Omega))
                self.Encoder.update()
                self.time = utime.ticks_diff(self.current_time, self.run_time)
                self.Omega = self.Encoder.get_speed(self.period/1e3) # [rpm]
                if self.dbg:
                    print('{:}, {:}, {:}, {:}\r\n'.format(self.time, self.Omega, self.Encoder.get_delta(), self.Encoder.period/1e3))
                else:
                    self.ser.write('{:}, {:}\r\n'.format(self.time, self.Omega))
                if self.ser.any():
                    val = self.ser.readline().decode('ascii')
                    if val == 'S':
                        self.ser.write('S')
                        self.Motor.disable()
                        sys.exit()
                    else:
                        pass
                if utime.ticks_diff(self.current_time, self.run_time) >= 3e3:
                    # if self.dbg:
                    #     print('{:}, {:}\r\n'.format(self.time[self.i], self.Ω[self.i]))
                    # else:
                    #     self.ser.write('{:}, {:}, {:}, {:}\r\n'.format(self.time[self.i], self.Ω[self.i], self.Encoder.get_delta(), self.Encoder.period/1e3))
                    self.ser.write('S')
                    self.Motor.disable()
                    sys.exit()
        
            elif self.state == self.S3_GET_K:
                if self.ser.any():
                    ## User inputted proportional gain
                    self.User_Kp = float(self.ser.readline().decode('ascii'))
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    if self.dbg:
                        self.ser.write('{:}\r\n'.format(self.User_Kp))
                    
            elif self.state == self.S4_GET_Omega_ref:
                if self.ser.any():
                    ## User desired angular velocity [rpm]
                    self.Omega_ref = int(self.ser.readline().decode('ascii'))
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    if self.dbg:
                        self.ser.write('{:}\r\n'.format(self.Omega_ref))
        
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.period)   
            
            
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the Data class is transitioning to
        '''
        self.state = newState
        


##############################################################################
##############################################################################        
##############################################################################

        

class Encoder:
    '''
    @brief Sets-up the encoder on the Nucelo and reads the encoder's position
    @details Using the user defined PPR, max rpm, and counter size, an appropriate time 
             period is calculated to run this class to read the encoder's position.
             Overflow (max counter range to 0) and underflow (0 to max counter range)
             is accounted for by checking the difference between readings and 
             making the appropriate corrections if it is > 0.5*counter_range.
    '''
    
    def __init__(self, timer, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm):      
        '''
        @brief Defines the encoder on the Nucelo, the time period to run, and initializes the finite state machine
        @param timer        Timer object used with pyb.Timer()
        @param pin1         Pin object 1 for the connection of the encoder to the Nucelo
        @param pin1_ch      Channel on the Timer object used for Pin object 1
        @param pin2         Pin object 2 for the connection of the encoder to the Nucelo
        @param pin2_ch      Channel on the Timer object used for Pin object 2
        @param counter_size Size of the processor's timer in bits
        @param PPR          Pulses per revolution of the encoder
        @param rpm          Maximum revolutions per minute of the motor
        '''
        
        ## Timer object
        self.timer = timer
        ## Pin1 object
        self.pin1 = pin1
        ## Pin1 Channel Object
        self.pin1_ch = pin1_ch
        ## Pin2 Object
        self.pin2 = pin2
        ## Pin2 Channel Object
        self.pin2_ch = pin2_ch
        ## Creates object for Pulses per Revolution
        self.PPR = PPR
        
        
        ## Defines the range of the counter before overflow
        self.counter_range = 2**(counter_size) - 1  
        
        ## Sets-up a timer object for the encoder
        self.tim = pyb.Timer(timer)
        #print('tim = pyb.Timer(' + str(timer) +')');
        
        ## Defines the timer object to counter every pulse for the desired counter range
        self.tim.init(prescaler = 0, period = self.counter_range)
        #print('tim.init(prescaler = 0, period = {:})'.format(period))
        
        ## Defines the timer channel for Pin object 1
        self.tim.channel(pin1_ch, pin = pin1, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin1_ch) + ', pin = pyb.Pin.cpu.' + str(pin1) + ', mode = pyb.Timer.ENC_AB)')
        
        ## Defines the timer channel for Pin object 2
        self.tim.channel(pin2_ch, pin = pin2, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin2_ch) + ', pin = pyb.Pin.cpu.' + str(pin2) + ', mode = pyb.Timer.ENC_AB)')
    
        ######################################################################        
        
        ## A constant defining the shortest time interval necessary to read the encoder's position without errors
        self.period_max = int((0.5*(2**counter_size) / (PPR*rpm/60))*1e3) # period necessary to run encoder in miliseconds
        
        ## Total displacement of the output shaft. This is the sum of all the "delta's" since the encoder was zeroed 
        self.position = 0
        
        ## Position of the encoder (in ticks) of the last calling of tim.counter() before the present position of the encoder
        self.previous_position = 0
        
        ## Constant used to alter current position 
        self.offset_position = 0
        

    def update(self):
        '''
        @brief Updates the encoder's total displaced position
        @details The current position (in ticks) is called and after calculating
                 the difference between the current and previous reading (accounting
                 for under- and overflow)* the total displacement is updated.
                 
                 *If the difference is greater than 0.5*counter_range,
                 the delta is corrected, depending on whether it is positive or
                 negative, by subtracting or adding the counter_range, respectively.
        '''
        ## Reading of the encoder (in ticks) from the tim.counter() command
        self.current_position = self.tim.counter() + self.offset_position
        ## Constant used to determine how many overflows the tick counter has passed
        self.i = math.floor(self.current_position / self.counter_range)
        if self.current_position > self.counter_range*self.i:
            self.current_position = self.current_position - self.counter_range*self.i
        # print('Previous position:    {:}'.format(self.previous_position))
        # print('Got current position: {:0.0f}'.format(self.current_position))
        
        ## Difference in the tick value of the current position from the previous reading of the encoder
        self.delta = self.current_position - self.previous_position
        # print('Got delta')
        
        self.previous_position = self.current_position
        
        if abs(self.delta) > 0.5*self.counter_range:
            #print('Bad delta = {:}'.format(self.delta))
            if self.delta > 0:
                self.delta -= self.counter_range
                #print('Corrected delta')
            elif self.delta < 0:
                self.delta += self.counter_range
        else:
            #print('Good delta')
            pass
        
        self.position += self.delta
        #self.get_position()
            
        
    def get_delta(self):
        '''
        @brief Returns the difference between the two most recent calls of the "update" function
        '''
        return self.delta
    
    
    def get_position(self):
        '''
        @brief Displays the current encoder position (in ticks) and total shaft displacement (in degrees) to the user interface
        '''
        # return self.position*360/self.PPR
        print('Encoder counter: {:}'.format(self.current_position))
        print('Total Shaft position: {:0.2f} deg'.format(self.position*360/(self.PPR)))
        #print('------------------------------------------------')
        
    
    def get_speed(self, delta_t):
        '''
        @brief Calculates the output shaft's current angular velocity [rpm] based on a given time step [sec]
        '''
        return (self.get_delta()*60/self.PPR) / delta_t # [rpm]
    
    def set_position(self, new_position):
        '''
        @brief Sets encoder counter and total shaft position to a given value. For example, passing in 0 will zero the encoder position.
        '''
        self.current_position = int(new_position*self.PPR/360)
        self.position = self.current_position
        self.i = math.floor(self.current_position / self.counter_range)
        if self.current_position > self.counter_range*self.i:
            self.current_position = self.current_position - self.counter_range*self.i
        self.previous_position = self.current_position
        self.offset_position = int(new_position*self.PPR/360)
        # Reset counter
        self.tim = pyb.Timer(self.timer)
        self.tim.init(prescaler = 0, period = self.counter_range)
        self.tim.channel(self.pin1_ch, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(self.pin2_ch, pin = self.pin2, mode = pyb.Timer.ENC_AB)
       
        
        
###############################################################################
###############################################################################
###############################################################################



class Motor:
    ''' 
    @brief This class implements a motor driver for the ME305/405 board. 
    '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer, dbg):
        ''' 
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin    A pyb.Pin object to use as the input to half bridge 1.
        @param IN1_ch     A channel object for IN1_pin.
        @param IN2_pin    A pyb.Pin object to use as the input to half bridge 2.
        @param IN2_ch     A channel object for IN2_pin.
        @param timer      A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin. 
        @param dbg        1 = Print debug statements, 0 = No debug statements
        '''
        
        ## Object for pinA15 on the Nucleo. Initialized as push-pull (on/off)
        self.pin15 = pyb.Pin(nSLEEP_pin, mode = pyb.Pin.OUT_PP)
        
        ## Object for IN1 using a Nucleo pin to control Motor 1
        self.IN1 = pyb.Pin(IN1_pin)
        
        ## Object for IN2 using a Nucleo pin to control Motor 2
        self.IN2 = pyb.Pin(IN2_pin)
        
        ## Object for timer that controls motor inputs
        self.timer = pyb.Timer(timer, freq=20000)
        
        ## Object for the timer channel used with IN1
        self.timer_ch_IN1 = self.timer.channel(IN1_ch, pyb.Timer.PWM, pin = self.IN1)
        
        ## Object for the timer channel used with IN2
        self.timer_ch_IN2 = self.timer.channel(IN2_ch, pyb.Timer.PWM, pin = self.IN2)
        
        ## Object for debug statements
        self.dbg = dbg
        
        if self.dbg:
            print('Creating a motor driver')
            
        self.timer_ch_IN1.pulse_width_percent(0)
        self.timer_ch_IN2.pulse_width_percent(0)
        
    def enable (self):
        '''
        @brief Initializes motors by setting pinA15 to high() 
        '''
        self.pin15.high()
        if self.dbg:
            print ('Enabling Motor')

    def disable (self):
        '''
        @brief Disables motors by setting pinA15 to low()
        '''
        self.pin15.low()
        if self.dbg:
            print ('Disabling Motor')

    def set_duty (self, duty):
        '''
        @brief   Sets the duty cycle to be sent to the motors (affects motor speed)
        @details This method sets the duty cycle to be sent to the motors (affects
                 motor speed) to the given level. Positive values cause effort 
                 in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        if duty > 0:
            self.timer_ch_IN2.pulse_width_percent(0)
            self.timer_ch_IN1.pulse_width_percent(duty)
            if self.dbg:
                print('Motor spinning forwards (CCW)')
        elif duty < 0:
            self.timer_ch_IN1.pulse_width_percent(0)
            self.timer_ch_IN2.pulse_width_percent(abs(duty))
            if self.dbg:
                print('Motor spinning backwards (CW)')
        else:
            self.timer_ch_IN1.pulse_width_percent(duty)
            self.timer_ch_IN2.pulse_width_percent(duty)
            if self.dbg:
                print('Motor coasting to a stop')
       
        
        
###############################################################################
###############################################################################
###############################################################################



class ClosedLoop:
    '''
    @brief Calculates a closed-loop feedback equation to determine the necessary motor duty cycle
    '''

    def __init__(self, User_Kp, Omega_ref, delta_t):
        '''
        @brief Creates objects for the user defined Kp, Ω_ref, and Δt
        '''
                
        ## DC Voltage from Nucleo
        self.V_DC = 3.33 # DC Voltage
        
        ## Proportional gain
        self.Kp = User_Kp / self.V_DC
        
        ## Desired angular velocity [rpm]
        self.Omega_ref = Omega_ref
        
        ## Time period for updating encoder
        self.delta_t = delta_t 
        
        
    def run(self, Omega):
        '''
        @brief Calculates the necessary motor duty cycle based on closed-loop, negative feedback
        '''
        
        ## Actual angular velocity [rpm]
        self.Omega = Omega
        ## Motor duty cycle calculated based on closed-loop, negative feedback
        L = int(self.Kp*(self.Omega_ref - self.Omega))
        if L > 100:
            L = 100
        elif L < -100:
            L = -100
        else:
            pass
        return L
        
    def set_Kp(self, User_Kp):
        '''
        @brief Sets the proportional gain, Kp, to a specified value
        '''
        self.Kp = User_Kp / self.V_DC
        
    def get_Kp(self):
        '''
        @brief Returns the current value for Kp
        '''
        return self.Kp


        
        
        
        
