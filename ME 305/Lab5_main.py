'''
@file Lab5_main.py

Sets-up task1 which runs Lab5_BLE.py until the user aborts the program.

*Lab5_main.py must be named main.py on the Nucleo for it to run on boot-up
'''

from Lab5_BLE import BLE_FSM

## Create Task 1 Object to initialize BLE.py
task1 = BLE_FSM(0) # 0 = no debug, 1 = debug

while True:
    task1.run()

