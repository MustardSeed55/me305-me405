'''
@file Elevator_main.py
@brief Calls Elevator.py to run tasks
'''

from Elevator import Button, MotorDriver, Elevator
        
# Creating objects to pass into task constructors
print('Objects for task 1:')
## Motor Object for task 1
motor_1     = MotorDriver()    # elevator motor
## Floor 1 button for task 1
button1_1   = Button('PB6')    # button for floor 1
## Floor 2 button for task 1
button2_1   = Button('PB7')    # button for floor 2
## Arrived at floor 1 button for task 1
first_1     = Button('PB8')    # button indicating floor 1 has been reached
## Arrived at floor 2 button for task 1
second_1    = Button('PB9')    # button indicating floor 2 has been reached

print('Objects for task 2:')
## Motor Object for task 2
motor_2     = MotorDriver()    # elevator motor
## Floor 1 button for task 2
button1_2   = Button('PB6')    # button for floor 1
## Floor 2 button for task 2
button2_2   = Button('PB7')    # button for floor 2
## Arrived at floor 1 button for task 2
first_2     = Button('PB8')    # button indicating floor 1 has been reached
## Arrived at floor 2 button for task 2
second_2    = Button('PB9')    # button indicating floor 2 has been reached



## Task 1 object
task1 = Elevator(0.1, motor_1, button1_1, button2_1, first_1, second_1) # Will also run constructor
## Task 2 object
task2 = Elevator(0.1, motor_2, button1_2, button2_2, first_2, second_2) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
#    task2.run()
