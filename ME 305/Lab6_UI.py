'''
@file Lab6_UI.py

This file controls the user interface for Task_Controller.py which runs a DC motor
with a Nucleo pyboard.

A user is prompted for their desired angular velocity [rpm] and proportional gain.

The program can be stopped at any time by entering a captial 'S'.

This file outputs a plot of the motor's angular velocity with respect to time 
as well as a CSV file containing all the data.
'''

import keyboard
import matplotlib.pyplot as plt
import numpy as np
import serial
import sys

## Sets-up serial communication with the Nucleo
ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)

## Print debug statements
dbg = 0
    
print('\n-----------------------------------------------------\n' +
      'User commands:\n' +
      '\tW = Desired angular velocity, Ω_desired [rpm]\n' + 
      '\tK = Proprotional gain, K\n' +
      '\tG = Start colletcing encoder data\n' +
      '\tS = Stop collecting data (quit)\n' +
      'Note: Program will automatically stop collecting data after 3 seconds.\n')

## Command entered by the user
input_cmd = ''

## Counter to know when Kp has been entered
i = 0

## Counter to know when Omega_ref has been entered
n = 0

# Keep prompting user for a command until 'G' is entered. Won't run the motor until Omega_ref and Kp have been entered
while input_cmd != 'G':
    input_cmd = input('Enter command: ')
    if input_cmd == 'K':
        ser.write(input_cmd.encode('ascii'))
        ## User input for proportional gain
        Kp = input('Enter proportional gain, K: ')
        ser.write('{:}'.format(Kp).encode('ascii'))
        i = 1
        if dbg:
            val = ser.readline().decode('ascii')
            print('Nucleo value = {:}'.format(val))
    elif input_cmd == 'W':
        ser.write(input_cmd.encode('ascii'))
        ## User desired angular velocity [rpm]
        Omega_ref = input('Enter desired angular velocity [rpm]: ')
        ser.write('{:}'.format(Omega_ref).encode('ascii'))
        n = 1
        if dbg:
            val = ser.readline().decode('ascii')
            print('Nucleo value = {:}'.format(val))
    elif input_cmd == 'G':
        if i == 0 and n == 0:
            print('Please enter K and Ω_desired values')
            input_cmd = ''
        elif i == 0:
            print('Please enter a K value')
            input_cmd = ''
        elif n == 0:
            print('Please enter a Ω_desired value')
            input_cmd = ''
        else:
            ser.write(input_cmd.encode('ascii'))
    elif input_cmd == 'S':
        ser.close()
        sys.exit()
    else:
        print('Invalid Command')


## A list to store all the time values collected from Task_Data.py
t = []
## A list to store all the angular velocity values collected from Task_Data.py
Omega = []

while True:
    # Nucleo is sending strings in the form 'time, position\r\n'
    ## Rceives the string of time and position values from the Nucleo
    data_string = ser.readline().decode('ascii')    # Decode serial communication using Ascii standards
    if data_string == 'S':
        break
    elif keyboard.is_pressed('S'):                  # If 'S' is entered in console, tell Nucleo to stop collecting data
        ser.write('S'.encode('ascii'))
        break
    else:
        ## Strip string sent from Nucleo of '\r\n' characters
        data_strip = data_string.strip()            # Strip string sent from Nucleo of '\r\n' characters
    
        ## A list of the data_string once it is split into two parts: time and position
        data_list = data_strip.split(',')           # Split string sent from Nucleo after a comma
        if dbg:
            print('List = {:}'.format(data_list))
        t.append(float(data_list[0]) / 1e3)         # Convert time portion of data_list into a floating point number in units of [sec]
        Omega.append(float(data_list[1]))               # Convert position portion of data_list into a floating point number

# Close serial communication with Nucleo
ser.close()

# Using matplotlib.pyplot
plt.plot(t, Omega, 'k')
plt.xlabel('Time, t [sec]')
plt.ylabel('Angular Velocity, Ω [rpm]')
plt.legend(['Ω_ref = {:}\nKp = {:}'.format(Omega_ref, Kp)], loc = 'center right')
print('Plot created')

# Create a .csv file from time and position lists
## Creates a tuple of time and position values to be used with np.colum_stack()
data_tup = (t, Omega)
## Creates an array of 2 columns with as many rows as data points that were collected
data_CSV = np.column_stack(data_tup)
np.savetxt('Motor_Data.csv', data_CSV, fmt = '%f', delimiter = ',')
print('Motor_Data.csv created in current folder')
    
    
