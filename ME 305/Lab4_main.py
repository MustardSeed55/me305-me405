'''
@file Lab4_main.py

This file automatically runs upon the Nucleo booting-up and calls two classes 
within the Task_Data.py file:

1) It prompts the Encoder() class to set-up the encoder to be available through 
the pyboard as well as start reading the encoder's position.

2) It also prompts the Data() class which collects the encoder's position using
Encoder.get_position() at an interval of 5 Hz for 10 seconds or until the user
types 'S' into the console. This data is then sent back to the user-end to be
used for plotting and creating a .csv file.

*Lab4_main.py must be named main.py on the Nucleo for it to run on boot-up
'''
import pyb
from Task_Data import Data
from Task_Data import Encoder

## Pin object 1
pin1 = pyb.Pin(pyb.Pin.cpu.A6)

## Pin Object 2
pin2 = pyb.Pin(pyb.Pin.cpu.A7)

## Task 1 object for setting-up and running the encoder: (timer, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm)
task1 = Encoder(3, pin1, 1, pin2, 2, 16, 700, 310)

## Task 2 object for collecting encoder data
task2 = Data(task1)

while True:
    task1.run()
    task2.run()