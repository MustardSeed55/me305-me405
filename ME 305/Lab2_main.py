'''
@file Lab2_main.py
@brief Calls Lab2.py to run tasks
'''

import time
from Lab2 import LED_Controller

## Task 1 Object (Period [sec], LED_Type [1 = Real, 2 = Virtual])
task1 = LED_Controller(3, 1)

## Task 2 Object (Period [sec], LED_Type [1 = Real, 2 = Virtual])
task2 = LED_Controller(1, 0) 


## Determines the desired time to end running the tasks in seconds \
t_end = time.time() + 20    # End time is 20 seconds after the start
while time.time() < t_end:  # Will run tasks 1 and 2 until the end time has been reached
    task1.run()
    task2.run()