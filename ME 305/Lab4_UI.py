'''
@file Lab4_UI.py

This file is the user interface for collecting the position of an encoder that is 
connected to a Nucleo pyboard.

The Nucleo will only start collecting data once 'G' is entered into the console.
Data will then be collected by the Nucleo at 5 Hz for 10 seconds unless a user
types 'S' into the console before 10 seconds has passed.

A plot and .csv file are created using matplotlib.pyplot and numpy.savetxt, respectively.
'''
import time
import keyboard
import matplotlib.pyplot as plt
import numpy as np
import serial

## Sets-up serial communication with the Nucleo
ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)

    
print('-----------------------------------------------------\n' +
      'User commands:\n' +
      '\tG = Start colletcing encoder data\n' +
      '\tS = Stop collecting data\n' +
      'Note: Program will automatically stop collecting data after 10 seconds.\n')

## Command entered by the user
input_cmd = ''

# Keep prompting user for a command until 'G' is entered
while input_cmd != 'G':
    input_cmd = input('Enter command: ')
    if input_cmd == 'G':
        ser.write(input_cmd.encode('ascii'))
    elif input_cmd == 'S':
        pass
    else:
        print('Invalid Command')

## Number of data points collected (collecting at 5 Hz)
i = 1           
## Collecting data at every multiple of 200 miliseconds (5 Hz)
multiple = 1  
## Prints 'Collecting data...' after 3 seconds * update_signal
update_signal = 1
## Time when data task starts collecting data in miliseconds
t_start = round(time.time()*1e3)         
## End time is 10 seconds after the start  
t_end = round(time.time()*1e3) + 10e3      

while round(time.time()*1e3) < t_end:      # Run loop until 10 seconds has passed
    if keyboard.is_pressed('S'):        # If 'S' is entered in console, tell Nucleo to stop collecting data
        ser.write('S'.encode('ascii'))
        break
    elif round(round(time.time()*1e3) - t_start) >= multiple*200: # Increment i every 200 miliseconds (5 Hz)
        if i == multiple:    
            i += 1
            multiple += 1
            # Display 'Collecting data...' every 3 seconds
            if round(round(time.time()*1e3) - t_start) >= update_signal*3000:
                print('Collecting data...')
                update_signal += 1



## A list to store all the time values collected from Task_Data.py
t = []
## A list to store all the position values collected from Task_Data.py
position = []


if i < 51:
    ## Determines how many lines to read from serial communication (51 if user doesn't type 'S')
    idx = i
else:
    idx = 51
## Counting index for a 'for' loop
n = 0
for n in range(idx):
    # Nucleo is sending strings in the form 'time, position\r\n'
    ## Rceives the string of time and position values from the Nucleo
    data_string = ser.readline().decode('ascii')    # Decode serial communication using Ascii standards
    data_string.strip()                             # Strip string sent from Nucleo of '\r\n' characters
    
    ## A list of the data_string once it is split into two parts: time and position
    data_list = data_string.split(',')              # Split string sent from Nucleo after a comma
    t.append(float(data_list[0]) / 1e3)             # Convert time portion of data_list into a floating point number in units of [sec]
    position.append(float(data_list[1]))            # Convert position portion of data_list into a floating point number

# Close serial communication with Nucleo
ser.close()

# Using matplotlib.pyplot
plt.plot(t, position)
plt.xlabel('Time, t [sec]')
plt.ylabel('Position, θ [deg]')
print('Plot created')

# Create a .csv file from time and position lists
## Creates a tuple of time and position values to be used with np.colum_stack()
data_tup = (t, position)
## Creates an array of 2 columns with as many rows as data points that were collected
data_CSV = np.column_stack(data_tup)
np.savetxt('Lab4_Data.csv', data_CSV, fmt = '%f', delimiter = ',')
print('Lab4_Data.csv created in current folder')

