'''
@file Lab2.py

This file controls LED2 on a Nucelo board as well as a "Virtual" LED which is
simply a print() command. 

The Real LED brightens over a desired time period using Pulse Width Modulation (PWM)
while the Virtual LED turns on and off at a desired time period.

This file uses pyboard specific commands, such as utime, so it cannot be run 
in programs like Spyder unless all the pyb specific commands have been commented.
'''

import pyb
import utime
#import time

class LED_Controller:
    '''
    @brief      A finite state machine to control an LED
    @details    This class implements a finite state machine to control the
                operation of a Real or a Virtual LED. The Real LED brightens 
                using PWM and the Virtual LED turns on and off using print()
                commands.
    '''
    
    ## Constant defining State 0: Initializing Program
    S0_Init         = 0
    
    ## Constant defining State 1: Increment LED brightness
    S1_Increment    = 1
    
    def __init__(self, period, LED_Type):
        '''
        @brief          Creates a LED Controller object with period and LED type as inputs
        @param period   The user desired time period for an LED to turn on and off
        @param LED_Type The user desired LED type: Real or Virtual 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_Init
        
        ## A class attribute "copy" of the LED_Type object
        self.LED_Type = LED_Type
         
        ## Defines the state of the Virtual LED
        self.Vir_LED = True
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()  # The number of miliseconds since arbitrary starting pont
        #self.start_time = time.time()      # The number of seconds since Jan 1. 1970
    
        # Divides the period by 100 if using a Real LED for PWM
        if self.LED_Type == 1:
                ## The interval of time, in miliseconds, for one cycle to make the LED blink
                self.period = period*10 # Want the Real LED to run 100 times for one period
        else:
            self.period = period*1000   
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.period)
        
        ## Counting index for Real LED
        self.i = 0
        
        ## Defines pinA5 as a CPU pin
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        
        ## Creates a timer object for LED2 (on the Nucelo) to cycle at 20 kHz
        self.tim2 = pyb.Timer(2, freq = 20000)
        
        ## Creates a timer channel using the tim2 timer object to perform pulse width modulation on pinA5
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin = self.pinA5)
        
        
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    Real LED: At a user desired time period, LED2 on the Nucleo py-board
                    will grow in brightness according to a saw-tooth pattern.
                    Virtual LED: At a user desired time period, a print() command
                    will be executed to display "On" or "Off".
        '''
        
        # Current time relative to 1/1/1970 for each iteration of the task
        #self.current_time = time.time() # Updating current timestamp
        
        ## Current time in miliseconds with respect to an arbitrary starting point
        self.current_time = utime.ticks_ms() 
        if utime.ticks_diff(self.current_time, self.next_time) >= 0 or self.runs == 0:
            
            if self.LED_Type == 1:   # Real LED on Nucleo
                if self.state == self.S0_Init:
                    #print(str(self.runs) + ' State 0: Initialize     - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                    self.transitionTo(self.S1_Increment)
                elif self.state == self.S1_Increment:
                    #print(str(self.runs) + ' State 1: Increment      - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                    if self.i <= 100:
                        #print(self.runs)
                        #print('i = {:}'.format(self.i) + 'Elapsed Time = {:0.2f} seconds'.format(utime.ticks_diff(self.current_time, self.start_time)))
                        self.t2ch1.pulse_width_percent(self.i)
                        #print('Pulse width = {:}%'.format(self.i))
                        self.i += 1
                    else:
                        self.i = 0
    
            elif self.LED_Type == 0: # Virtual LED
                if self.Vir_LED:
                    #print(str(self.runs) + ' Virtual LED ON          - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                    print('Virtual LED ON')
                    #print('Time: {:} ms'.format(utime.ticks_diff(self.current_time, self.start_time)))
                    self.Vir_LED = False
                else:
                    #print(str(self.runs) + ' Virtual LED OFF         - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                    print('Virtual LED OFF')
                    #print('Time: {:} ms'.format(utime.ticks_diff(self.current_time, self.start_time)))
                    self.Vir_LED = True
    
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.period)
                
                
                
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the LED is in (Initialize or Incrementing Brightness)
        '''
        self.state = newState
        