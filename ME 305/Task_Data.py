'''
@file Task_Data.py

This file is prompted to run by Lab4_main.py* and contains two classes:

1) Data(): collects the encoder's position using
Encoder.get_position() at an interval of 5 Hz for 10 seconds or until the user
types 'S' into the console. This data is then sent back to the user-end to be
used for plotting and creating a .csv file.

2) Encoder(): sets-up the encoder to be available through 
the pyboard as well as start reading the encoder's position.

*Lab4_main.py must be named main.py on the Nucleo for it to run on boot-up
'''

import pyb
from pyb import UART
import utime
import sys

class Data:
    '''
    @brief   Collects encoder's position at 5 Hz for 10 seconds
    @details Collects the encoder's position using Encoder.get_position() at an 
             interval of 5 Hz for 10 seconds or until the user types 'S' into 
             the console.
    '''
    
    ## Constant defining State 0: Initializing Program
    S0_INIT           = 0
    
    ## Constant defining State 1: Wait for command from user interface
    S1_WAIT_FOR_CMD   = 1
    
    ## Constant defining State 2: Get encoder position and timestamp
    S2_GET_DATA       = 2
    
    ## Constant defining State 3: Send encoder position and time to user
    S3_SEND_DATA      = 3
    
    
    def __init__(self, task1_Encoder):
        '''
        @brief Sets-up Finite State Machine and serial communication with user
        @param task1_Encoder Inputting the Encoder class as a parameter so Encoder
                             specific functions can be called.
        '''
        
        ## The state to run on the next iteration of the task
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## A constant defining the starting time of the program in miliseconds
        self.start_time = utime.ticks_ms()  # The number of miliseconds since arbitrary starting point

        ## A constant defining the time period this program will wait before re-running
        self.period = 200 # 0.2 sec
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.period)

        ## Sets-up serial port so Nucelo can communicate with a user's computer
        self.ser = UART(2)
        
        ## Creates object for encoder class
        self.Encoder = task1_Encoder
        
        ## Timestamp for encoder position
        self.time = [0]
        
        ## Encoder position
        self.position = [0]
        
        ## Counter index to document length of data arrays
        self.i = 0
        
        ## Counter index to ser.write() data
        self.n = 0

    def run(self):
        '''
        @brief Runs at 4 states: (0) Initialize, (1) Wait for user command, (2) Get encoder data, (3) Send data to user
        @details Upon boot-up, Data() automatically goes into the WAIT_FOR_CMD state. When the user enters 'G' into the console,
                 Data() goes to the GET_DATA state where Encoder.get_position() is used to get the Encoder's position at an
                 interval of 5 Hz for 10 seconds. After 10 seconds, or if the user enters 'S' into the console, Data() goes
                 to the SEND_DATA state where the run() function cycles every 1 milisecond and sends the collected data over
                 a serial communication with the user. After all the data is sent, the program closes.
        '''
        
        ## Current time (in miliseconds) since an arbitrary start time (same instant as variable self.start_time)
        self.current_time = utime.ticks_ms() 
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0 or self.runs == 0):
            if self.state == self.S0_INIT:
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                    
            elif self.state == self.S1_WAIT_FOR_CMD:
                #print('State 1: Wait for Command')
                if self.ser.any():
                    val = self.ser.readline().decode('ascii')
                    if val == 'G':
                        self.transitionTo(self.S2_GET_DATA)
                        ## Documents the time when Data() starts to collect data so data collection can stop after 10 seconds
                        self.run_time = utime.ticks_ms()
                    if val == 'S':
                        self.transitionTo(self.S3_SEND_DATA)
            elif self.state == self.S2_GET_DATA:
                self.time.append(utime.ticks_diff(self.current_time, self.run_time))
                self.position.append(self.Encoder.get_position())
                self.i += 1
                if self.ser.any():
                    val = self.ser.readline().decode('ascii')
                    if val == 'S':
                        self.transitionTo(self.S3_SEND_DATA)
                    else:
                        pass
                if utime.ticks_diff(self.current_time, self.run_time) >= 10e3:
                    self.transitionTo(self.S3_SEND_DATA)
                    
            elif self.state == self.S3_SEND_DATA:
                self.period = 1
                self.ser.write('{:}, {:}\r\n'.format(self.time[self.n], self.position[self.n]))
                self.n += 1
                if self.n > self.i:
                    sys.exit()
        

            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.period)   
            
            
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the Data class is transitioning to
        '''
        self.state = newState
        

##############################################################################
##############################################################################        
        

class Encoder:
    '''
    @brief Sets-up the encoder on the Nucelo and reads the encoder's position
    @details Based on the user's desired pins on the Nucelo board, this class
             defines the appropriate timer and channel for each pin. Using the 
             user defined PPR, max rpm, and counter size, an appropriate time 
             period is used to run this class to read the encoder's position.
             Overflow (max counter range to 0) and underflow (0 to max counter range)
             is accounted for by checking the difference between readings and 
             making the appropriate corrections if it is > 0.5*counter_range.
             A finite state machine is used to run the encoder in two different 
             states: Intialize and Update.
    '''
    
    ## Constant defining State 0: Initializing Program
    S0_Init     = 0
    
    ## Constant defining State 1: Update Encoder
    S1_Update   = 1
    
    ## Constant defining total encoder position
    position = 0
    
    ## Constant defining the previous position of the encoder
    previous_position = 0
    
    ## Constant defining the difference between the current reading and the previous reading
    delta = 0
    
    
    def __init__(self, timer, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm):      
        '''
        @brief Defines the encoder on the Nucelo, the time period to run, and initializes the finite state machine
        @param timer        Timer object used with pyb.Timer()
        @param pin1         Pin object 1 for the connection of the encoder to the Nucelo
        @param pin1_ch      Channel on the Timer object used for Pin object 1
        @param pin2         Pin object 2 for the connection of the encoder to the Nucelo
        @param pin2_ch      Channel on the Timer object used for Pin object 2
        @param counter_size Size of the processor's timer in bits
        @param PPR          Pulses per revolution of the encoder
        @param rpm          Maximum revolutions per minute of the motor
        '''
        
        ## Defines the range of the counter before overflow
        self.counter_range = 2**(counter_size) - 1  
        
        ## Sets-up a timer object for the encoder
        self.tim = pyb.Timer(timer)
        #print('tim = pyb.Timer(' + str(timer) +')');
        
        ## Defines the timer object to counter every pulse for the desired counter range
        self.tim.init(prescaler = 0, period = self.counter_range)
        #print('tim.init(prescaler = 0, period = {:})'.format(period))
        
        ## Defines the timer channel for Pin object 1
        self.tim.channel(pin1_ch, pin = pin1, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin1_ch) + ', pin = pyb.Pin.cpu.' + str(pin1) + ', mode = pyb.Timer.ENC_AB)')
        
        ## Defines the timer channel for Pin object 2
        self.tim.channel(pin2_ch, pin = pin2, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin2_ch) + ', pin = pyb.Pin.cpu.' + str(pin2) + ', mode = pyb.Timer.ENC_AB)')
    
        ######################################################################        

        ## The state to run on the next iteration of the task.
        self.state = self.S0_Init   
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## A constant defining the starting time of the program in miliseconds
        self.start_time = utime.ticks_ms()  # The number of miliseconds since arbitrary starting point
        #self.start_time = time.time()      # The number of seconds since Jan 1. 1970 
        
        ## A constant defining the shortest time interval necessary to read the encoder's position without errors
        self.period_max = int((0.5*(2**counter_size) / (PPR*rpm/60))*1e3) # period necessary to run encoder in miliseconds
        if self.period_max > 100: # 100 milisecond
            ## A constant defining the time period this program will wait before re-running. If period_max is less than 1 milisecond, this program has to use utime.ticks.us()
            self.period = 100
        elif self.period_max < 100 and self.period_max > 1:
            self.period = self.period_max
        else:
            print('ERROR: period is too small. Need to use utime.ticks.us()')
            print('-------Program Ended-------')
            sys.exit()
        #self.period = 0.1
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.period)
        #self.next_time = self.start_time + self.period
        
        ## Creates object for PPR to be used in get_position()
        self.PPR = PPR
        

    def run(self):
        '''
        @brief Runs the finite state machine of the encoder (S0: Initialize and S1: Update)
        '''
        ## Current time (in miliseconds) since an arbitrary start time (same instant as variable self.start_time)
        self.current_time = utime.ticks_ms() 
        #self.current_time = time.time()
        if utime.ticks_diff(self.current_time, self.next_time) >= 0 or self.runs == 0:
        #if (self.current_time >= self.next_time) or self.runs == 0:
            
            if self.state == self.S0_Init:
                #print(str(self.runs) + ' State 0: Initialize     - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                self.transitionTo(self.S1_Update)
                
            elif self.state == self.S1_Update:
                #print(str(self.runs) + ' State 1: Update         - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                self.update()
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.period)   
            #self.next_time += self.period
            
            
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the Encoder is transitioning to
        '''
        self.state = newState


    def update(self):
        '''
        @brief Reads the position of the encoder by calling get_delta() and then adding that value onto the total position
        '''
        ## Total displacement of the encoder. This is the sum of all the "delta's" since the encoder was zeroed 
        self.position += self.get_delta()
        #self.get_position()
            
        
    def get_delta(self):
        '''
        @brief Computes the difference between the current encoder position and the previous position
        @details Calls tim.counter() to read the encoder's current position. 
                 The previous position is then subtracted from this value to 
                 get the "delta". If this difference is greater than 0.5*counter_range,
                 the delta is corrected, depending on whether it is positive or
                 negative, by subtracting or adding the counter_range, respectively.
        '''
        
        ## Reading of the encoder (in ticks) from the tim.counter() command
        self.current_position = self.tim.counter()
        #print('Previous position:    {:}'.format(self.previous_position))
        #print('Got current position: {:0.0f}'.format(current_position))
        
        ## Difference in the tick value of the current position from the previous reading of the encoder
        self.delta = self.current_position - self.previous_position
        #print('Got delta')
        
        ## Position of the encoder (in ticks) of the last calling of tim.counter() before the present position of the encoder
        self.previous_position = self.current_position
        if abs(self.delta) > 0.5*self.counter_range:
            #print('Bad delta')
            if self.delta > 0:
                self.delta -= self.counter_range
                #print('Corrected delta')
                return self.delta
            elif self.delta < 0:
                self.delta += self.counter_range
                #print('Corrected delta')
                return self.delta
        else:
            #print('Good delta')
            return self.delta
    
    
    def get_position(self):
        '''
        @brief Displays the current encoder position in ticks and total shaft displacement in degrees to the user interface
        '''
        return self.position*360/self.PPR
        #print('Encoder counter: {:}'.format(self.current_position))
        #print('Total Shaft position: {:0.2f} deg'.format(self.position*360/(self.counter_range)))
        #print('------------------------------------------------')
        
        
    def set_position(self, NewValue):
        '''
        @brief Sets encoder counter and total shaft position to a given value. For example, passing in 0 will zero the encoder position.
        '''
        ## Total displacement of the encoder. This is the sum of all the "delta's" since the encoder was zeroed 
        self.position = NewValue*self.PPR/360