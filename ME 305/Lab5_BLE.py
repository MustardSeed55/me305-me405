'''
@file Lab5_BLE.py

This file is prompted to run by Lab5_main.py*, interacts with Lab5_App.aia,
and contains the following classes:
    
    1) BLE_FSM: Runs as a finite-state-machine to turn an LED on a Nucelo board
                on, off, or blink at a desired frequency
    2) BLE_Driver: Contains all the pyboard specific UART() commands for reading
                and writing serial communication strings as well as setting-up
                the LED pin on the Nucleo

*Lab5_main.py must be named main.py on the Nucleo for it to run on boot-up
'''

import pyb
import utime

class BLE_FSM:
    '''
    @brief Runs as a finite-state-machine to turn an LED on a Nucelo board on, off, or blink at a desired frequency
    @details The finite-state-machine starts in the initialization state (INIT)
             and automatically goes to the LED CONTROL state where this file
             tells the LED what to do based on an input from a user app using
             serial communication.
    '''
    
    ## Constant defining State 0: Initializing Program
    S0_INIT           = 0
    
    ## Constant defining State 1: Control state of LED (on or off) 
    S1_LED_CONTROL    = 1

    
    def __init__(self, dbg):
        '''
        @brief Initializes the finite-state-machine
        @details Sets-up the finite-state-machine timers and period, sets the initial
                 state to INIT, and creates an object for the BLE_Driver() class
                 so it's functions can be used in the FSM.
        @param dbg This is a bool (1/0 or True/False). If it is true, different
                   print() statements will be enabled throughout the code to track
                   its progress.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()  # The number of miliseconds since arbitrary starting pont

        ## A constant defining the time period this program will wait before re-running
        self.period = 100 # 0.1 sec
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.period)
        
        ## Index to determine if LED is on or off
        self.i = 1
        
        ## Create object for debugging 
        self.dbg = dbg
        
        ## Create object for BLE_Driver
        self.BLE_Driver = BLE_Driver(dbg)
        
        ## Variable to store user input: False = turn LED on or off, int = blink at that frequency
        self.val = False
        
    def run(self):
        '''
        @brief Loops through the FSM until a command is sent from the user's app
        @details This FSM continually loops until a command is sent from the user's
                 app. The only commands that will do anything are a positive integer
                 and the words "on" and "off". The integer value tells the Nucelo
                 LED to blink at that frequency, "on" tells the LED to turn on, 
                 and "off" tells the LED to turn off.
        '''
        
        ## Current time (in miliseconds) since an arbitrary start time (same instant as variable self.start_time)
        self.current_time = utime.ticks_ms() 
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0 or self.runs == 0):
            
            if self.state == self.S0_INIT:
                self.transitionTo(self.S1_LED_CONTROL)
                
            if self.state == self.S1_LED_CONTROL:
                if self.BLE_Driver.Any():
                    self.val = self.BLE_Driver.read()
                if self.dbg:
                    print('FSM val: {:}'.format(self.val))
                if self.val != False:
                    self.period = int((1/self.val)*1e3)
                    if self.i == 0:
                        self.BLE_Driver.off()
                        self.i = 1
                    elif self.i == 1:
                        self.BLE_Driver.on()
                        self.i = 0
            
                
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.period) 
    
            
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the FSM is transitioning to
        '''
        self.state = newState

##############################################################################


class BLE_Driver:
    '''
    @brief Holds all the UART commands and sets-up the LED pin on the Nucleo
    @details This class sets-up a mode 3 UART() port to communicate with a bluetooth
             antenna on the Nucleo which sends and receives commands from a mobile app.
             PinA5 is also initialized on the Nucleo which controls the board's LED.
    @param dbg This is a bool (1/0 or True/False). If it is true, different
               print() statements will be enabled throughout the code to track
               its progress.
    '''
    
    def __init__(self, dbg):
        '''
        @brief Sets-up UART() port and pinA5
        @details UART() port 3 with a buadrate of 9600 is set-up and pinA5 on the 
                 Nucleo is initialized which controls the board's LED.
        @param dbg This is a bool (1/0 or True/False). If it is true, different
                   print() statements will be enabled throughout the code to track
                   its progress.
       '''
        
        ## Sets-up serial port so Nucelo can communicate with a user's bluetooth antenna
        self.ser = pyb.UART(3, 9600)
        
        ## Initialize Nucleo pin A5 to turn off or on
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        ## Create object for debugging 
        self.dbg = dbg
        
    def Any(self):
        '''
        @brief Returns "True" if there are characters on the serial communication, "False" if there aren't
        '''
        if self.ser.any():
            return True
        else:
            return False
            
    def read(self):
        '''
        @brief Reads the serial communication string and then acts on the inputed command
        @details A variable val is used to store the serial communication string. If it is 
                 an integer, a string will be sent to the user's app telling them
                 what frequency the LED is blinking at and val is returned to 
                 BLE_FSM() to be used to cycle at the desired frequency. If val 
                 is not an integer and if it is "on" or "off", the LED will be 
                 commanded to turn on or off. 
        '''
        ## Stores the string data sent over serial communication
        self.val = self.ser.readline().decode()
        if self.dbg:
            print(self.val)
        try:
            self.val = int(self.val)
            self.write('LED Blinking at {:} Hz'.format(self.val))
            if self.dbg:
                print('Driver val: {:}'.format(self.val))
            return self.val
        except:
            if self.val == 'on':
                self.on()
                self.write('LED ON')
            elif self.val == 'off':
                self.off()
                self.write('LED OFF')
            return False
                
    def write(self, string):
        '''
        @brief Uses serial communication to write strings to a mobile app
        '''
        self.ser.write(string)
        
    def on(self):
        '''
        @brief Turns the LED (pinA5) on
        '''
        self.pinA5.high()
        
    def off(self):
        '''
        @brief Turns the LED (pinA5) off
        '''
        self.pinA5.low()
