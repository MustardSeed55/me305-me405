'''
@file Elevator.py

This file simulates an Elevator (a finite-state-machine) using Python.

The user has a two buttons to decide what floor they want to go to.

There is a limit switch at either end of travel of the elevator to stop 
the motor as well as variables representing the floor the elevator is
currently at.
'''

from random import choice
import time

class Elevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN      = 1
    
    ## Constant defining State 2
    S2_STOPPED_FLOOR_1  = 2
    
    ## Constant defining State 3
    S3_MOVING_UP        = 3
    
    ## Constant defining State 4
    S4_STOPPED_FLOOR_2  = 4

    
    def __init__(self, interval, motor, button_1, button_2, first, second):
        '''
        @brief      Creates an Elevator object with multiple button inputs.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.motor = motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the Button_1 object
        self.button_1 = button_1
        
        ## A class attribute "copy" of the Button_2 object
        self.button_2 = button_2
        
        ## The button object used for the first floor being reached
        self.first = first
        
        ## The button object used for the second floor being reached
        self.second = second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    The elevator automatically goes to floor 1 upon initialization.
                    Once at either floor, the elevator will only move if the button
                    for the opposite floor is pushed.
        '''
        ## Calculates the current time (relative to 1/1/1970) for each iteration of the task
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 (Initialize)       - Elapsed Time: {:0.2f} seconds'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.motor.Down()
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 (Moving Down)      - Elapsed Time: {:0.2f} seconds'.format(self.curr_time - self.start_time))
                # Run State 1 
                # print('\tfirst    = ' + str(self.first))
                # print('\tsecond   = ' + str(self.second))
                # print('\tbutton_1 = ' + str(self.button_1))
                # print('\tbutton_2 = ' + str(self.button_2))
                if self.first.getButtonState():
                    self.transitionTo(self.S2_STOPPED_FLOOR_1)
                    self.motor.Stop()
                    self.button_1.defineButtonState('False')
                    self.button_2.defineButtonState('False')
                    self.second.defineButtonState('False')
                    
            elif(self.state == self.S2_STOPPED_FLOOR_1):
                print(str(self.runs) + ' State 2 (Stopped Floor 1)  - Elapsed Time: {:0.2f} seconds'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                # print('\tfirst    = ' + str(self.first))
                # print('\tsecond   = ' + str(self.second))
                # print('\tbutton_1 = ' + str(self.button_1))
                # print('\tbutton_2 = ' + str(self.button_2))
                if self.button_2.getButtonState():
                    self.transitionTo(self.S3_MOVING_UP)
                    self.motor.Up()
                    self.first.defineButtonState('False')
            
            elif(self.state == self.S3_MOVING_UP):
                print(str(self.runs) + ' State 3 (Moving Up)        - Elapsed Time: {:0.2f} seconds'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                # print('\tfirst    = ' + str(self.first))
                # print('\tsecond   = ' + str(self.second))
                # print('\tbutton_1 = ' + str(self.button_1))
                # print('\tbutton_2 = ' + str(self.button_2))
                if self.second.getButtonState():
                    self.transitionTo(self.S4_STOPPED_FLOOR_2)
                    self.motor.Stop()
                    
            elif(self.state == self.S4_STOPPED_FLOOR_2):
                print(str(self.runs) + ' State 4 (Stopped Floor 2)  - Elapsed Time: {:0.2f} seconds'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                # print('\tfirst    = ' + str(self.first))
                # print('\tsecond   = ' + str(self.second))
                # print('\tbutton_1 = ' + str(self.button_1))
                # print('\tbutton_2 = ' + str(self.button_2))
                if self.button_1.getButtonState():
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.motor.Down()
                    self.second.defineButtonState('False')

            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the elevator is in
        '''
        self.state = newState
        
    # def __str__(self):
    #     return '{self}'
        
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary elevator rider to go to a desired floor. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A bool representing the state of the button.
        '''
        return choice([True, False])  # returns either 0 or 1
    
    def defineButtonState(self, state):
        '''
        @brief      Defines the button state.
        @details    Sets a button to a specific state: True or False
        @param state  The desired state of the button: True or False
        @return     A bool representing the state of the button.
        '''
        return state


class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                go up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor CCW (upwards)
        '''
        print('Motor moving CCW (upwards)')
    
    def Down(self):
        '''
        @brief Moves the motor CW (downwards)
        '''
        print('Motor moving CW (downwards)')
    
    def Stop(self):
        '''
        @brief The motor is turned off
        '''
        print('Motor Stopped')
    

