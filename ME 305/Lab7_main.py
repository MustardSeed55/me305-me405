'''
@file Lab7_main.py

Calls Task_Encoder.py to control a DC motor connected to a Nucleo pyboard

*Lab7_main.py must be named main.py on the Nucleo for it to run on boot-up
'''

from Lab7_Task_Controller import Encoder
from Lab7_Task_Controller import Motor
from Lab7_Task_Controller import Controller
import pyb

''' Define Encoder parameters '''

## Pyboard timer object
timer_ENC = 8

## Pin object 1
pin1 = pyb.Pin(pyb.Pin.cpu.C6)

## Channel of timer for Pin Object 1
pin1_ch = 1

## Pin Object 2
pin2 = pyb.Pin(pyb.Pin.cpu.C7)

## Channel of timer for Pin Object 2
pin2_ch = 2

## Size of counter (bits). The Nucelo has a 16-bit counter
counter_size = 16 # bit size

## Pulse per revolution for user's encoder
PPR = 4000 # Pulse per revolution

## Maximum revolutions per minute for motor
rpm = 12400 # maximum rpm of motor (Note: output shaft will be 4x faster)

''' Define Motor parameters '''

## Pin object used to enable the motor
nSLEEP_pin = pyb.Pin.cpu.A15

## Pin object for input 1 to half-bridge 
IN1_pin = pyb.Pin.cpu.B0

## Pin object for input 2 channel to half-bridge
IN1_ch = 3

## Pin object for input 2 to half-bridge 
IN2_pin = pyb.Pin.cpu.B1

## Pin object for input 2 channel to half-bridge
IN2_ch = 4

## Create the timer object used for PWM generation
timer_MOT = 3

## Create the debug object. 1 = print debug statements, 0 = no debug statements
dbg = 0


''' Define program tasks '''

## Task Object that sets-up the encoder
task1 = Encoder(timer_ENC, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm)

## Task Object that sets-up the motor
task2 = Motor(nSLEEP_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer_MOT, dbg)

## Task Object that runs the controller which controls the motor and encoder
task3 = Controller(task1, task2, dbg)


while True:
    task3.run()