'''
@file Create_CSV.py
'''

import numpy as np
import matplotlib.pyplot as plt

## List to store time values
time = []
## List to store angular velocity values
velocity = []
## List to store angular position values
position = []
## Counter to populate time, velocity, and position variables
i = 0

# ref = open('reference.csv')

# while True:
#     # Read a line of data. It should be in the format 't,v,x\n' but when the
#     # file runs out of lines the lines will return as empty strings, i.e. ''
#     line = ref.readline()
    
#     # If the line is empty, there are no more rows so exit the loop
#     if line == '':
#         break
    
#     # If the line is not empty, strip special characters, split on commas, and
#     # then append each value to its list.
#     else:
#         (t,v,x) = line.strip().split(',') 
#         if float(t) < 0.050*i+0.001 and float(t) > 0.050*i-0.001:
#             time.append(float(t))
#             velocity.append(float(v))
#             position.append(float(x))
#             i += 1
#         else:
#             pass

# ref.close()

while i <= 300:
    time.append(0.05*i)
    if i <= 80:
        velocity.append(1500)
        position.append(velocity[i]*360/60*time[i])
        i += 1
    elif i > 80 and i <= 140:
        velocity.append(0)
        position.append(position[80])
        i += 1
    elif i > 140 and i <= 160:
        velocity.append(-3000)
        position.append(position[80] + velocity[i]*360/60*(time[i] - time[140]))
        i += 1
    elif i > 160 and i <= 220:
        velocity.append(0)
        position.append(position[160])
        i += 1
    elif i > 220 and i <= 240:
        velocity.append(1500/20*(i-220))
        position.append(position[220] + velocity[i]*360/60*(time[i] - time[220]))
        i += 1
    elif i > 240 and i <= 260:
        velocity.append(velocity[240] - 1500/20*(i-240))
        position.append(position[i-1] + velocity[i]*360/60*(time[i] - time[240]))
        i += 1
    elif i > 260:
        velocity.append(0)
        position.append(position[260])
        i += 1

## Temporary tuple to combine time, velocity, and position arrays
data_tup = (time, velocity, position)
## Organizes data_tup to create CSV format with 3 columns
data_CSV = np.column_stack(data_tup)
np.savetxt('Lab7_reference.csv', data_CSV, fmt = '%f', delimiter = ',')
print('Lab7_reference.csv created in current folder')


# To make the example complete, plot the data as a subplot.
plt.figure(1)

# Velocity first
plt.subplot(2,1,1)
plt.plot(time,velocity)
plt.ylabel('Velocity [RPM]')

# Then position
plt.subplot(2,1,2)
plt.plot(time,position)
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')