'''
@file Task_Encoder.py

This file setups and updates the encoder connected to a user's motor.

It will read the encoder's position at the necessary time period to ensure accuracy.
'''

import pyb
import utime
import sys

class Encoder:
    '''
    @brief Sets-up the encoder on the Nucelo and reads the encoder's position
    @details Based on the user's desired pins on the Nucelo board, this class
             defines the appropriate timer and channel for each pin. Using the 
             user defined PPR, max rpm, and counter size, an appropriate time 
             period is used to run this class to read the encoder's position.
             Overflow (max counter range to 0) and underflow (0 to max counter range)
             is accounted for by checking the difference between readings and 
             making the appropriate corrections if it is > 0.5*counter_range.
             A finite state machine is used to run the encoder in two different 
             states: Intialize and Update.
    '''
    
    ## Constant defining State 0: Initializing Program
    S0_Init     = 0
    
    ## Constant defining State 1: Update Encoder
    S1_Update   = 1
    
    ## Constant defining total encoder position
    position = 0
    
    ## Constant defining the previous position of the encoder
    previous_position = 0
    
    ## Constant defining the difference between the current reading and the previous reading
    delta = 0
    
    
    def __init__(self, timer, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm):      
        '''
        @brief Defines the encoder on the Nucelo, the time period to run, and initializes the finite state machine
        @param timer        Timer object used with pyb.Timer()
        @param pin1         Pin object 1 for the connection of the encoder to the Nucelo
        @param pin1_ch      Channel on the Timer object used for Pin object 1
        @param pin2         Pin object 2 for the connection of the encoder to the Nucelo
        @param pin2_ch      Channel on the Timer object used for Pin object 2
        @param counter_size Size of the processor's timer in bits
        @param PPR          Pulses per revolution of the encoder
        @param rpm          Maximum revolutions per minute of the motor
        '''
        
        ## Defines the range of the counter before overflow
        self.counter_range = 2**(counter_size) - 1  
        
        ## Sets-up a timer object for the encoder
        self.tim = pyb.Timer(timer)
        #print('tim = pyb.Timer(' + str(timer) +')');
        
        ## Defines the timer object to counter every pulse for the desired counter range
        self.tim.init(prescaler = 0, period = self.counter_range)
        #print('tim.init(prescaler = 0, period = {:})'.format(period))
        
        ## Defines the timer channel for Pin object 1
        self.tim.channel(pin1_ch, pin = pin1, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin1_ch) + ', pin = pyb.Pin.cpu.' + str(pin1) + ', mode = pyb.Timer.ENC_AB)')
        
        ## Defines the timer channel for Pin object 2
        self.tim.channel(pin2_ch, pin = pin2, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin2_ch) + ', pin = pyb.Pin.cpu.' + str(pin2) + ', mode = pyb.Timer.ENC_AB)')

        ######################################################################        

        ## The state to run on the next iteration of the task.
        self.state = self.S0_Init   
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## A constant defining the starting time of the program in miliseconds
        self.start_time = utime.ticks_ms()  # The number of miliseconds since arbitrary starting pont
        #self.start_time = time.time()      # The number of seconds since Jan 1. 1970 
        
        ## A constant defining the shortest time interval necessary to read the encoder's position without errors
        self.period_max = int((0.5*(2**counter_size) / (PPR*rpm/60))*1e3) # period necessary to run encoder in miliseconds
        if self.period_max > 1: # 1 milisecond
            ## A constant defining the time period this program will wait before re-running if the shortest time interval necessary is greater than 1 milisecond. If period_max is less than 1 milisecond, this program has to use utime.ticks.us()
            self.period = 1
        else:
            print('ERROR: period is too small. Need to use utime.ticks.us()')
            print('-------Program Ended-------')
            sys.exit()
        #self.period = 0.1
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.period)
        #self.next_time = self.start_time + self.period
        

    def run(self):
        '''
        @brief Runs the finite state machine of the encoder (S0: Initialize and S1: Update)
        '''
        ## Current time (in miliseconds) since an arbitrary start time (same instant as variable self.start_time)
        self.current_time = utime.ticks_ms() 
        #self.current_time = time.time()
        if utime.ticks_diff(self.current_time, self.next_time) >= 0 or self.runs == 0:
        #if (self.current_time >= self.next_time) or self.runs == 0:
            
            if self.state == self.S0_Init:
                #print(str(self.runs) + ' State 0: Initialize     - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                self.transitionTo(self.S1_Update)
                
            elif self.state == self.S1_Update:
                #print(str(self.runs) + ' State 1: Update         - Elapsed Time = {:0.2f} seconds'.format(self.current_time - self.start_time))
                self.update()
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.period)   
            #self.next_time += self.period
            
            
    def transitionTo(self, newState):
        '''
        @brief           Updates the variable defining the next state to run
        @param newState  The new state the Encoder is transitioning to
        '''
        self.state = newState


    def update(self):
        '''
        @brief Reads the position of the encoder by calling get_delta() and then adding that value onto the total position
        '''
        ## Total displacement of the encoder. This is the sum of all the "delta's" since the encoder was zeroed 
        self.position += self.get_delta()
        #self.get_position()
            
        
    def get_delta(self):
        '''
        @brief Computes the difference between the current encoder position and the previous position
        @details Calls tim.counter() to read the encoder's current position. 
                 The previous position is then subtracted from this value to 
                 get the "delta". If this difference is greater than 0.5*counter_range,
                 the delta is corrected, depending on whether it is positive or
                 negative, by subtracting or adding the counter_range, respectively.
        '''
        #test_position = [0, 2, 2**2, 2**3, 2**4, 2**5, 2**6, 2**7, 2**8, 2**9, 2**10, 2**11, 2**12, 2**13, 2**14, 2**15, 2**16-1, 1, 2, 2**2, 2**3, 2**4, 2**5, 2**6]
        #test_position = [0, 2, 2**2, 2**3, 2**4, 2**5, 2**6, 2**7, 2**8, 2**7, 2**6, 2**5, 2**4, 2**3, 2**2, 2, 0, 2**16-1, 2**15, 2**14]
        #current_position = test_position[self.i]
        #self.i += 1
        #print('tim.counter()')
        
        ## Reading of the encoder (in ticks) from the tim.counter() command
        self.current_position = self.tim.counter()
        #print('Previous position:    {:}'.format(self.previous_position))
        #print('Got current position: {:0.0f}'.format(current_position))
        
        ## Difference in the tick value of the current position from the previous reading of the encoder
        self.delta = self.current_position - self.previous_position
        #print('Got delta')
        
        ## Position of the encoder (in ticks) of the last calling of tim.counter() before the present position of the encoder
        self.previous_position = self.current_position
        if abs(self.delta) > 0.5*self.counter_range:
            #print('Bad delta')
            if self.delta > 0:
                self.delta -= self.counter_range
                #print('Corrected delta')
                return self.delta
            elif self.delta < 0:
                self.delta += self.counter_range
                #print('Corrected delta')
                return self.delta
        else:
            #print('Good delta')
            return self.delta
    
    
    def get_position(self):
        '''
        @brief Displays the current encoder position in ticks and total shaft displacement in degrees to the user interface
        '''
        print('Encoder counter: {:}'.format(self.current_position))
        print('Total Shaft position: {:0.2f} deg'.format(self.position*360/(self.counter_range)))
        #print('------------------------------------------------')
        
        
    def set_position(self, NewValue):
        '''
        @brief Sets encoder counter and total shaft position to a given value. For example, passing in 0 will zero the encoder position.
        '''
        ## Total displacement of the encoder. This is the sum of all the "delta's" since the encoder was zeroed 
        self.position = NewValue*self.counter_range/360
