'''
@file Lab7_UI.py

This file controls the user interface for Task_Controller.py which runs a DC motor
with a Nucleo pyboard.

A user is prompted for their desired angular velocity [rpm] and proportional gain.

The program can be stopped at any time by entering a captial 'S'.

This file outputs a plot of the motor's angular velocity with respect to time 
as well as a CSV file containing all the data.
'''

import keyboard
import matplotlib.pyplot as plt
import numpy as np
import serial
import sys
import time

## Sets-up serial communication with the Nucleo
ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 0.1)

## Print debug statements
dbg = 0

## Reference time
time_ref = []
## Reference angular velocity [rpm]
velocity = []
## Reference angular position [deg]
position = []
## Counter so only data at multiples of t = 0.05 sec is read
k = 0

## Open reference CSV file
ref = open('Lab7_reference.csv')

while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    ## Reads each line of the reference CSV file
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t,v,x) = line.strip().split(',') 
        if float(t) < 0.050*k+0.001 and float(t) > 0.050*k-0.001:
            time_ref.append(float(t))
            velocity.append(float(v))
            position.append(float(x))
            k += 1
        else:
            pass
ref.close()

print('\n-----------------------------------------------------\n')
print('Sending Ω_ref...')
ser.write('W'.encode('ascii'))
time.sleep(0.1) # Complete timeout before next ser.write()
## Counter for while loop
n = 0
## Length of reference velocity array
length = len(velocity)
while n < length:   
    ser.write('{:}'.format(velocity[n]).encode('ascii'))
    time.sleep(0.1) # Complete timeout before next ser.write()
    if n == 50 or n == 100 or n == 150 or n == 200 or n == 250:
        print('...')
    if dbg:
        ## Placeholder variable to read the serial port
        val = ser.readline().decode('ascii')
        print('Nucleo value = {:}'.format(val))
    n += 1
time.sleep(0.1) # Complete timeout before next ser.write()
ser.write('S'.encode('ascii'))

val = ser.readline().decode('ascii')
if val == 'G':
    print('Ω_ref received')
else:
    print('ERROR: Ω_ref NOT received!')
    ser.close()
    sys.exit()

print('\n-----------------------------------------------------\n' +
      'User commands:\n' +
      '\tK = Proprotional gain, K\n' +
      '\tG = Start colletcing motor data\n' +
      '\tS = Stop collecting data (quit)\n' +
      'Note: Program will automatically stop after data is collected.\n')

## Command entered by the user
input_cmd = ''

## Counter to know when Kp has been entered
i = 0

# Keep prompting user for a command until 'G' is entered. Won't run the motor until Omega_ref and Kp have been entered
while input_cmd != 'G':
    input_cmd = input('Enter command: ')
    if input_cmd == 'K':
        ser.write(input_cmd.encode('ascii'))
        ## User input for proportional gain
        Kp = input('Enter proportional gain, K: ')
        ser.write('{:}'.format(Kp).encode('ascii'))
        i = 1
        if dbg:
            val = ser.readline().decode('ascii')
            print('Nucleo value = {:}'.format(val))
    elif input_cmd == 'G':
        if i == 0:
            print('Please enter a K value')
            input_cmd = ''
        else:
            ser.write(input_cmd.encode('ascii'))
    elif input_cmd == 'S':
        ser.close()
        sys.exit()
    else:
        print('Invalid Command')


## A list to store all the time values collected from Task_Data.py
t = []
## A list to store all the angular velocity values collected from Task_Controller.py
Omega = []
## A list to store all the angular position values collected from Task_Controller.py
Theta = []

while True:
    # Nucleo is sending strings in the form 'time, position\r\n'
    ## Rceives the string of time and position values from the Nucleo
    data_string = ser.readline().decode('ascii')    # Decode serial communication using Ascii standards
    if dbg:
        print('Data string = ' + data_string)
    if data_string == 'S':
        break
    elif keyboard.is_pressed('S'):                  # If 'S' is entered in console, tell Nucleo to stop collecting data
        ser.write('S'.encode('ascii'))
        break
    else:
        ## Strip string sent from Nucleo of '\r\n' characters
        data_strip = data_string.strip()            # Strip string sent from Nucleo of '\r\n' characters
    
        ## A list of the data_string once it is split into two parts: time and position
        data_list = data_strip.split(',')           # Split string sent from Nucleo after a comma
        if dbg:
            print('List = {:}'.format(data_list))
        t.append(float(data_list[0]) / 1e3)         # Convert time portion of data_list into a floating point number in units of [sec]
        Omega.append(float(data_list[1]))           # Convert position portion of data_list into a floating point number
        Theta.append(float(data_list[2]))

# Close serial communication with Nucleo
ser.close()

# Calculate performance J
n = 0
## Reference value for summation in J calculation
J_0 = 0
## Value of the summation in J calculation
J_sum = 0
while n < length:
    J_0 = J_sum
    J_sum = J_0 + velocity[n] - Omega[n]**2 + (position[n] - Theta[n])**2
    n += 1
## Performance metric
J = (1/length)*J_sum
print('J = {:d}'.format(round(J)))


# Using matplotlib.pyplot
plt.figure(1)
# Velocity subplot
plt.subplot(2,1,1)
plt.plot(t, Omega, 'k', time_ref, velocity, 'k--')
plt.grid(True)
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
plt.yticks([-4000, -3000, -2000, -1000, 0, 1000, 2000])
plt.ylabel('Ang. Vel., Ω [rpm]')
plt.legend(['Ω', 'Ω_ref'])
# Position subplot
plt.subplot(2,1,2)
plt.plot(t, Theta, 'k', time_ref, position, 'k--')
plt.grid(True)
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
plt.yticks([0, 20000, 40000, 60000])
plt.xlabel('Time, t [s]')
plt.ylabel('Position, θ [deg]')
plt.legend(['θ', 'θ_ref'])
print('Plot created')


# Create a .csv file from time and position lists
## Creates a tuple of time and position values to be used with np.colum_stack()
data_tup = (t, Omega)
## Creates an array of 2 columns with as many rows as data points that were collected
data_CSV = np.column_stack(data_tup)
np.savetxt('Motor_Data.csv', data_CSV, fmt = '%f', delimiter = ',')
print('Motor_Data.csv created in current folder')
    
    
