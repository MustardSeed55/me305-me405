'''
@file mainpage.py

@mainpage Introduction
This is a portfolio of mechatronics projects by Dakota Baker. These labs are from the classes ME 305 & ME 405 at California Polytechnic State University in San Luis Obispo.

@ref page_ME305 \par
Source code can be found in this public repository: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/

@ref page_ME405 \par
Source code can be found in this public repository: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/

@author Dakota Baker
@date Last updated: March 17, 2021
################
################
################

@page page_ME305 ME 305: Introduction to Mechatronics
This class focused on basic functions using a Nucelo pyboard such as turning LED's on/off and controlling DC motors. 

Table of Contents:
- @subpage subpage_lab1
- @subpage subpage_Elevator
- @subpage subpage_lab2
- @subpage subpage_lab3
- @subpage subpage_lab4
- @subpage subpage_lab5
- @subpage subpage_lab6 
- @subpage subpage_lab7

################
################
################
@page subpage_lab1 Lab 1: Fibonacci Sequence
The goal for this lab was to create a python script that could calculate the number at a given index in the Fibonacci sequence.

Documentation can be found here: @ref lab1.py

The file itself can be found here: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/lab1.py
 
@author Dakota Baker 

@date September 26, 2020
################
@page subpage_Elevator Elevator Finite-State-Machine
This project simulates an elevator as a finite-state-machine. Inputs are two buttons
which tell the elevator which floor to go to and two buttons which indicate what floor
the elevator is currently on.

Documentation can be found here: @ref Elevator.py and @ref Elevator_main.py

The files themselves can be found here:
- Elevator.py: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Elevator.py
- Elevator_main.py: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Elevator_main.py

The state-transition-diagram used is shown below:
@image html Elevator_State_Transition_Diagram.png

@author Dakota Baker 

@date October 3, 2020
################
@page subpage_lab2 Lab 2: Cooperative LED Tasks
This file controls LED2 on a Nucelo board as well as a "Virtual" LED which is
simply a print() command. 

The Real LED brightens over a desired time period using Pulse Width Modulation (PWM)
while the Virtual LED turns on and off at a desired time period.

This file uses pyboard specific commands, such as utime, so it cannot be run 
in programs like Spyder unless all the pyb specific commands have been commented.

Documentation can be found here: @ref Lab2.py and @ref Lab2_main.py

The files themselves can be found here:
- Lab2.py: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab2.py
- Lab2_main.py: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab2_main.py

The state-transition-diagram used is shown below:
@image html Lab2_STD.png

@author Dakota Baker 

@date October 10, 2020
################
@page subpage_lab3 Lab 3: Encoder Interaction
This file reads the current position of an encoder and depending on the user input
zeros the encoder position, displays the motor shaft's total displacement (in degrees),
and displays the difference (delta) between the two most recent readings of the encoder.  

This file uses pyboard specific commands for the Nucelo, such as utime and uart, 
so it cannot be run in programs like Spyder unless all the pyb specific commands 
have been commented.

There are three files necessary for this project:
- Lab3_main.py: Defines and runs the encoder and user interface tasks
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab3_main.py

- Task_Encoder.py: Sets-up the encoder on the pyboard and continuously reads the encoder's position
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Task_Encoder.py

- Task_UI.py: Controls the user interface. Prompts user commands and displays the result of each command
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Task_UI.py


The state-transition-diagrams used are shown below:

Encoder FSM:
@image html Lab3_FSM_Encoder.png

User Interface FSM:
@image html Lab3_FSM_User.png

@author Dakota Baker 

@date October 21, 2020
################
@page subpage_lab4 Lab 4: Get Encoder Position Through UI & Use Data
This project collects 51 data points of the enocder's position (5 Hz over 10 seconds).
This data is then used to create a plot and .csv file of the encoder's angular position over time.

There are three files necessary for this project:
- Lab4_UI.py: Runs on the user interface (ex: Spyder).
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab4_UI.py

- Lab4_main.py: Provides the neccessary input parameters and prompts the Encoder and Data classes within Task_Data.py to run.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab4_main.py

- Task_Data.py: Contains the Data() (runs as a Finite State Machine) and Encoder() classes. Collects data from the encoder at 5 Hz for 10 seconds.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Task_Data.py

The Task Diagram of how the three files interact is shown below:
@image html TD_Lab4.png


The Data() and Encoder() classes run as Finite State Machines. Their state-transition-diagrams used are shown below:

Encoder
@image html Lab3_FSM_Encoder.png

Data
@image html STD_TaskData.png


An example output plot is shown below. This shows the encoder spun by hand for 10 seconds:
@image html Lab4_Plot.png


An example CSV file is shown here (time [sec], position [deg]):
- https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab4_Data.csv

@author Dakota Baker 

@date October 31, 2020
################
@page subpage_lab5 Lab 5: Control LED Through Mobile App
This project controls the pinA5 LED on the Nucleo board (like Lab 2), but
this time uses a mobile app for commands. Additionally, the app will display
text telling the user the state of the LED: on, off, blinking at __ Hz.

There are three files necessary for this project:
- Lab5_main.py: This file prompts Lab5_BLE.py to run intil the user aborts the program.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab5_main.py

- Lab5_BLE.py: This file runs as a finite-state-machine to control the state of the LED. BLE stands for Bluetooth Low Energy.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab5_BLE.py

- Lab5_App.aia: This file can be downloaded by a mobile device and used as an app. It sends commands to the Nucelo to control the LED.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab5_App.aia

The Task Diagram of how the three files interact is shown below:
@image html Lab5_TaskDiagram.png


The state-transition-diagram of the BLE_FSM() class is shown below:
@image html Lab5_BLE_STD.png


@author Dakota Baker 
@date November 10, 2020
################
@page subpage_lab6 Lab 6: Motor Control
This project controls a DC motor on the ME305/405 board. 

The motor is controlled by the user interface Lab6_UI.py which interacts with Task_Controller.py. 
One enters their desired angular velocity [rpm] and proportional gain, K, for the controller. 
After 3 seconds (after the motor comes to steady-state), the motor is turned off and a plot and 
CSV file are created.

Here are the necessary files for this project:
- Lab6_UI.py: This is the front-end user interface. Using software like Spyder, one connects with the Nucleo board through serial communication.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab6_UI.py
  
- Lab6_main.py: This file runs on the Nucleo and intiates Task_Controller.py when the Nucelo boots-up.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab6_main.py
  
- Task_Controller.py: This file controls the motor. Runs as a finite state machine.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Task_Controller.py
  
An example CSV file with Ω_ref = 2000 rpm and K = 1 can be found here:
- https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Motor_Data.csv

Five plots, where K increases each run, are shown below. Notice that as K increases, the steady-state error decreases. 
However, increasing K can also cause more overshoot though that is not always the case. These responses can often vary
from run to run due to the nonlinearity (friction) in the motor.
@image html Lab6_1.png
@image html Lab6_2.png
@image html Lab6_3.png
@image html Lab6_4.png
@image html Lab6_5.png

The Task Diagram is shown below:
@image html Lab6_TaskDiagram.png

The State Transition Diagram for Task_Controller.py is shown below:
@image html Lab6_Controller_STD.png

@author Dakota Baker 
@date November 24, 2020
################
@page subpage_lab7 Lab 7: Motor Control (Reference Tracking)
This project controls a DC motor on the ME305/405 board using reference values from a CSV file.
The original angular velocity profile can be found here: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/reference.csv

However, due to the large friction force in the motor, a new velocity profile was created using Create_CSV.py.
Instead of 30 rpm, the max positive input became 1500. Instead of -60 rpm, the max negative input became -3000. Everything else
was then adjusted to match these values. Additionally, the original CSV file had to be adjusted so that only rows at a multiple
of t = 0.05 seconds were used. Thus, the reference CSV used for this project (created by Create_CSV.py) is Lab7_reference.csv. 
- Create_CSV.py can be found here: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Create_CSV.py


The motor is controlled by the user interface Lab7_UI.py which interacts with Lab7_Task_Controller.py. 
Upon starting the UI, the reference angular velocity (from Lab7_reference.csv) is sent to the Nucelo. 
The user can then enter their desired proportional gain, K, for the controller. After the data is collected, the performance
metric J is calculated, a plot is created, and a CSV file of the actual angular velocity output is created. 

Here are the necessary files for this project:

- Lab7_reference.csv: This file contains the reference values to control the motor. The columns are: time[ sec], velocity [rpm], position [deg].
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab7_reference.csv

- Lab7_UI.py: This is the front-end user interface. Using software like Spyder, one connects with the Nucleo board through serial communication.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab7_UI.py
  
- Lab7_main.py: This file runs on the Nucleo and intiates Lab7_Task_Controller.py when the Nucelo boots-up.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab7_main.py
  
- Lab7_Task_Controller.py: This file runs as a finite state machine on the Nucelo and controls the motor.
  - https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab7_Task_Controller.py
  
  
An example output CSV file with K = 0.11 can be found below. The columns are: time [sec], velocity [rpm], position [deg].
- https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20305/Lab7_Motor_Data.csv

Below is an example output plot with K = 0.11. The performance metric, J = 584194942. Notice that J is very large due to the 
large friction in the motor. This is illustrated in the plot where there is a ramp input. Notice that the motor struggles to 
turn when the input is very small.
@image html Lab7.png

The Task Diagram is shown below:
@image html Lab7_TaskDiagram.png

The State Transition Diagram for Lab7_Task_Controller.py is shown below:
@image html Lab7_Controller_STD.png

@author Dakota Baker 
@date December 2, 2020
################
################
################

@page page_ME405 ME 405: Mechatronics (In Progress)
This class focused on using a Nucleo pyboard and DC motors to create a platform that can balance a free-moving ball.

Table of Contents:
- @subpage subpage_ME405_lab1
- @subpage subpage_ME405_lab2
- @subpage subpage_ME405_lab3
- @subpage subpage_ME405_lab4
- @subpage subpage_ME405_lab5
- @subpage subpage_ME405_lab6
- @subpage subpage_ME405_lab7
- @subpage subpage_ME405_lab8
- @subpage subpage_ME405_lab9

################
################
################
@page subpage_ME405_lab1 Lab 1: Vending Machine

Documentation & Source Code
    * @ref ME405_Lab1.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab1.py

This program simulates a vending machine that runs as a finite state machine. 
Interactive messages appear in the user's Console and the user inputs commands 
to add money, get change, and purchase beverages.

State Transition Diagram:
@image html ME405_Lab1_STD.jpg width=600px height=600px


@author Dakota Baker 

@date January 12, 2021
################
################
@page subpage_ME405_lab2 Lab 2: Reaction Time

Documentation & Source Code
    * @ref ME405_Lab2.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab2.py

An LED blinks at a random time on the Nucleo. The user must then push 
the USER button on the Nucleo board. Using an external interrupt, a reaction 
time is calculated and then printed in the console.
Note: Can use the utime module or pyboard timers.

@author Dakota Baker 

@date January 25, 2021
################
@page subpage_ME405_lab3 Lab 3: USER Button Response

Documentation & Source Code
    * @ref ME405_Lab3.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab3.py
    * @ref ME405_Lab3_main.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab3_main.py
    * USER_Button.csv
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/USER_Button.csv

This project measures the response of the USER button on the Nucleo. An Analog-to-Digital Converter (ADC) is used on 
the Nucleo where the minimum 0 is 0 V and the maximum 4095 is 3.3 V. Serial communication between the user's computer
and the Nucleo is used to transmit data. This data is then exported to a CSV file and plotted. 
Note: A jumper cable is needed between the male PC_13 pin and the female PC_0 (A5) pin.

USER Button Response:
@image html ME405_Lab3_plot.png width=600px height=400px

@author Dakota Baker 

@date January 29, 2021
################
@page subpage_ME405_lab4 Lab 4: Temperature and I2C Communication

Documentation & Source Code
    * @ref mpc9808.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/mpc9808.py
        * Initializes I2C components and communication.
    * @ref ME405_Lab4_main.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab4_main.py
        * Runs on the Nucleo and calls mpc9808.py to collect ambient temperature data.
    * Temperature_Data.csv
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/Temperature_Data.csv
        * (time, Nucleo Internal Temp. [C], Ambient Temp. [C])
    * ME405_Lab4_Plot.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab4_Plot.py
        * Plots data from Temperature_Data.csv.

This project measures the internal tmperature of the Nucleo and the ambient temperature of a room,
using a mpc9808 sensor, every minute. The data is stored in Temperature_Data.csv which can then be
copied over to a user's computer. Upon start-up, the Nucleo will blink a green LED 3 times. If one
is using serial communication over Putty, use Ctrl-C to quit the program. If the Nucleo is running 
on its own, press the USER button to quit the program. After pressing the USER button, the green LED 
will light up for one second before turning off again to signal the end of the program. 

Note: Due to the minute delay between readings, it may take up to one minute before you see the LED 
light up after pressing the USER button.

A plot of the data is shown below. Measurements were taken from 8:10 AM to 4:30 PM.

Temperature vs. Time:
@image html ME405_Lab4_Plot.png width=600px height=400px

@author Dakota Baker 

@date February 8, 2021
################
@page subpage_ME405_lab5 Lab 5: Rotating Platform Dynamics

These are the hand-calculations for the dynamics of the platform-ball assembly as shown below:
@image html ME405_Platform.jpg width=700px height=550px

The @ref page_ME405_Lab5 page was created as a group assignment, but due to the may errors we originally made,
I re-wrote the equations. 

The referenced Matlab analysis can be found here: https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/Kinematics.mlx

@image html ME405_Lab5_Calcs1.jpg width=800px height=1035px
@image html ME405_Lab5_Calcs2.jpg width=800px height=1035px
@image html ME405_Lab5_Calcs3.jpg width=800px height=1035px
@image html ME405_Lab5_Calcs4.jpg width=800px height=1035px
@image html ME405_Lab5_Calcs5.jpg width=800px height=1035px


@author Dakota Baker 

@date February 22, 2021
################
@page subpage_ME405_lab6 Lab 6: Platform Dynamics Simulations

Documentation & Source Code
    * ME405_Lab6.mlx
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab6.mlx
    * ME405_Lab6_Simulation_OL.slx
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab6_Simulation_OL.slx
    * ME405_Lab6_Simulation_CL.slx
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab6_Simulation_CL.slx

This lab used the equations found in Lab 5 to run simulations of the platform-ball assembly with different 
starting conditions. The analysis uses a state space approach with x_dot, theta_dot_y, x, and theta_y being the state variables. 
Note: The instructor's equations were used and differed slightly from those calculated in Lab 5.

Open-Loop Simulation:
@image html ME405_Lab6_Simulation_OL.jpg width=580px height=150px

@image html ME405_Lab6_Figure1.png width=550px height=400px
Figure 1. The ball is initially at rest on a level platform directly above the center of gravity of the platform 
and there is no torque input from the motor. Because the system is at an equilibrium point, there is no motion.

@image html ME405_Lab6_Figure2.png width=550px height=400px
Figure 2. The ball is initially at rest on a level platform offset horizontally from the center of gravity of the 
platform by 5 [cm] and there is no torque input from the motor. Since the ball is off-center, the platform begins 
to tilt and the ball rolls further away from the center.

@image html ME405_Lab6_Figure3.png width=550px height=400px
Figure 3. The ball is initially at rest on a platform inclined at 5 [deg] directly above the center of gravity of 
the platform and there is no torque input from the motor. Because the platform is at an angle, the ball begins to 
roll away from the center and the platform continues to tilt.

@image html ME405_Lab6_Figure4.png width=550px height=400px
Figure 4. The ball is initially at rest on a level platform directly above the center of gravity of the platform and 
there is an impulse of 1 [N-m-s] applied by the motor. The input torque causes the platform to rotate counter clockwise 
and the ball to start rolling away from the center of the platform in the same direction as the platform is tilting.

Closed-Loop Simulation where T_x = K*x:
@image html ME405_Lab6_Simulation_CL.jpg width=580px height=200px

@image html ME405_Lab6_Figure5.png width=550px height=400px
Figure 5. The ball is initially at rest on a level platform offset horizontally from the center of gravity of the platform 
by 5 [cm] and there is no torque input from the motor. The system is in closed-loop and is unstable as illustrated by an 
ever-increasing trend.


@author Dakota Baker 

@date February 22, 2021
################
@page subpage_ME405_lab7 Lab 7: Platform Touchscreen

Documentation & Source Code
    * @ref ME405_Lab7_main.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab7_main.py
        * Initializes ME405_Lab7.py to read the platform's touchscreen output.
    * @ref ME405_Lab7.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab7.py
        * Reads the platform's touchscreen output using the TouchScreen class.
        

This lab used a TFT LCD Display (model: ER-TFT080-1) to track the motion of a ball on a rotating platform. 
The location is displayed as + or - from the center of the touchscreen in mm. This data will be used in 
future labs to control the motors to tilt the platform and thus balance the ball in the center of the platform.

When using the Scan_XYZ() function, which scans the z location (is there contact? True/False) then the y location
and then the x location, one can expect an average scan time of 1025 microseconds. If Scan_XYZ() is run continuously,
one can expect scan times around 960 microseconds.

Here is a picture of the platform with the ball that was used:
@image html ME405_Lab7_image.jpg width=600px height=800px

@author Dakota Baker 

@date February 24, 2021
################
@page subpage_ME405_lab8 Lab 8: Motor & Encoder Drivers

Documentation & Source Code
    * @ref ME405_Lab8_main.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab8_main.py
        * Initializes ME405_Lab8.py to run the motors and their respective encoders.
    * @ref ME405_Lab8.py
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab8.py
        * Contains the drivers for the ME305/405 board motors and encoders.
        

This lab set-up the drivers for the motors and encoders on the ME305/405 board. The only major difference
from ME305 is that the motor driver can detect when the nFault pin on the Nucleo's DRV8847 chip is triggered. 
When this pin is pulled low, the motors are disabled and the user is notified that there was an error. The user
can then continue operation or exit the program. 

@authors Dakota Baker, Jacob Lindberg 

@date March 8, 2021
################
@page subpage_ME405_lab9 Lab 9: Balance Ball on Platform

Documentation & Source Code (shared repository)
    * @ref ME405_main.py
        * https://bitbucket.org/jacoblindberg20/me405_termproject/src/master/ME405_main.py
        * Main script that runs all the other code. 
    * @ref ME405_Drivers.py
        * https://bitbucket.org/jacoblindberg20/me405_termproject/src/master/ME405_Drivers.py
        * Contains the drivers for the ME305/405 board motors and encoders.
    * @ref ME405_Touchscreen.py
        * https://bitbucket.org/jacoblindberg20/me405_termproject/src/master/ME405_Touchscreen.py
        * Contains the driver for the ME305/405 board touchscreen.  
    * @ref bno055.py
        * https://bitbucket.org/jacoblindberg20/me405_termproject/src/master/bno055.py
        * Calls bno055_base.py to set-up the imu sensor.
        * Note: This code was borrowed from an online source
    * @ref bno055_base.py
        * https://bitbucket.org/jacoblindberg20/me405_termproject/src/master/bno055_base.py
        * Used as an input to bno055.py to set-up the imu sensor.
        * Note: This code was borrowed from an online source

This lab tries to balance a ball on the ME305/405 platform. Unfortunately, we were not able to properly tune 
the closed-loop gains and thus the system is unstable using the Lab 6 gains:\n
K1 (x_dot)       = -0.05\n
K2 (theta_dot_y) = -0.02\n
K3 (x)           = -0.3\n
K4 (theta_y)     = -0.2

Video using the code and platform: https://youtu.be/UEZmOiC4CNA

@authors Dakota Baker, Jacob Lindberg 

@date March 17, 2021


#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #



################
@page page_ME405_Lab5 ME 405: Rotating Platform Dynamics

Below is the dynamics for the ME 405 rotating platform.
@image html ME405_Platform.jpg width=700px height=600px

Platform & Ball Kinematics:
@image html ME405_Lab5_Kinematics_1.jpg width=800px height=1035px
@image html ME405_Lab5_Kinematics_2.jpg width=800px height=1035px
@image html ME405_Lab5_Kinematics_3.jpg width=800px height=1035px

Platform & Ball Kinetics:
@image html ME405_Lab5_Kinetics_1.jpg width=800px height=1035px
@image html ME405_Lab5_Kinetics_2.jpg width=800px height=1035px
@image html ME405_Lab5_Kinetics_3.jpg width=800px height=1035px
@image html ME405_Lab5_Kinetics_4.jpg width=800px height=1035px
@image html ME405_Lab5_Kinetics_5.jpg width=800px height=1035px

Ball Kinetics Revisited:
@image html ME405_Lab5_Kinetics_6.jpg width=800px height=1035px
@image html ME405_Lab5_Kinetics_7.jpg width=800px height=1035px

@authors Dakota Baker, Jacob Lindberg, Brennen Irey

@date February 15, 2021
################
@page page_ME405_lab6 ME 405: Platform Dynamics Simulations

Documentation & Source Code
    * ME405_Lab6.mlx
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab6.mlx
    * ME405_Lab6_Simulation_OL.slx
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab6_Simulation_OL.slx
    * ME405_Lab6_Simulation_CL.slx
        * https://bitbucket.org/MustardSeed55/me305-me405/src/master/ME%20405/ME405_Lab6_Simulation_CL.slx

This lab used the equations found in Lab 5 to run simulations of the platform-ball assembly with different 
starting conditions. The analysis uses a state space approach with x_dot, theta_dot_y, x, and theta_y being the state variables. 
Note: The instructor's equations were used and differed slightly from those calculated in Lab 5.

Open-Loop Simulation:
@image html ME405_Lab6_Simulation_OL.jpg width=580px height=150px

@image html ME405_Lab6_Figure1.png width=550px height=400px
Figure 1. The ball is initially at rest on a level platform directly above the center of gravity of the platform 
and there is no torque input from the motor. Because the system is at an equilibrium point, there is no motion.

@image html ME405_Lab6_Figure2.png width=550px height=400px
Figure 2. The ball is initially at rest on a level platform offset horizontally from the center of gravity of the 
platform by 5 [cm] and there is no torque input from the motor. Since the ball is off-center, the platform begins 
to tilt and the ball rolls further away from the center.

@image html ME405_Lab6_Figure3.png width=550px height=400px
Figure 3. The ball is initially at rest on a platform inclined at 5 [deg] directly above the center of gravity of 
the platform and there is no torque input from the motor. Because the platform is at an angle, the ball begins to 
roll away from the center and the platform continues to tilt.

@image html ME405_Lab6_Figure4.png width=550px height=400px
Figure 4. The ball is initially at rest on a level platform directly above the center of gravity of the platform and 
there is an impulse of 1 [N-m-s] applied by the motor. The input torque causes the platform to rotate counter clockwise 
and the ball to start rolling away from the center of the platform in the same direction as the platform is tilting.

Closed-Loop Simulation where T_x = K*x:
@image html ME405_Lab6_Simulation_CL.jpg width=580px height=200px

@image html ME405_Lab6_Figure5.png width=550px height=400px
Figure 5. The ball is initially at rest on a level platform offset horizontally from the center of gravity of the platform 
by 5 [cm] and there is no torque input from the motor. The system is in closed-loop and is unstable as illustrated by an 
ever-increasing trend.


@author Dakota Baker 

@date February 22, 2021
'''